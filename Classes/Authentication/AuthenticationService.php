<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Authentication;

use Doctrine\DBAL\DBALException;
use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Service\AuthenticationSettings;
use RuntimeException;
use Doctrine\DBAL\Driver\Exception as DbException;
use IMATHUZH\OidcClient\Event\AuthenticationGetUserEvent;
use IMATHUZH\OidcClient\Service\Configuration;
use IMATHUZH\OidcClient\OAuth2\UserResource;
use IMATHUZH\OidcClient\Utility\Constants;
use IMATHUZH\OidcClient\Utility\AuthenticationStatus;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Crypto\PasswordHashing\InvalidPasswordHashException;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Authentication\AbstractAuthenticationService;

/**
 * OpenID Connect authentication service.
 *
 * @todo authentication via username & password
 */
class AuthenticationService extends AbstractAuthenticationService
{
    /**
     * @var Configuration Global extension configuration
     */
    protected Configuration $config;

    /**
     * @var ConnectionPool A cached connection pool for executing SQL queries
     */
    protected ConnectionPool $connectionPool;

    /**
     * @var string The login type 'fe' or 'be' set during initialization
     */
    protected string $loginType;

    /**
     * @var string The string 'frontend' or 'backend'
     */
    protected string $appType;

    /**
     * @var AuthenticationSettings global backend/frontend settings
     */
    protected AuthenticationSettings $authConfig;

    /**
     * @var string The name of the table with oidc users (frontend or backend)
     */
    protected string $oidcUserTable;

    /**
     * @var string The name of the table with group patterns (frontend or backend)
     */
    protected string $oidcGroupTable;

    /**
     * @var int The uid of the provider against which the user is authenticated
     */
    protected int $providerId = 0;

    /**
     * @var OidcAspect The aspect with the authentication result
     */
    protected OidcAspect $aspect;


    public function __construct(
        Context $context,
        ConnectionPool $connectionPool
    ) {
        $this->config = Configuration::getInstance();
        $this->connectionPool = $connectionPool;
        if ($context->hasAspect(Constants::ASPECT)) {
            $this->aspect = $context->getAspect(Constants::ASPECT);
        }
    }

    public function initAuth($mode, $loginData, $authInfo, $pObj)
    {
        parent::initAuth($mode, $loginData, $authInfo, $pObj);
        if ($pObj->loginType === 'FE') {
            $this->loginType = 'fe';
            $this->appType = 'frontend';
            $this->oidcUserTable = Constants::TABLE_FEUSER;
            $this->oidcGroupTable = Constants::TABLE_FEGROUP;
            $this->authConfig = $this->config->frontend();
        } else {
            $this->loginType = 'be';
            $this->appType = 'backend';
            $this->oidcUserTable = Constants::TABLE_BEUSER;
            $this->oidcGroupTable = Constants::TABLE_BEGROUP;
            $this->authConfig = $this->config->backend();
        }
    }


    /**
     * Finds a user.
     *
     * @return array|null
     * @throws RuntimeException
     */
    public function getUser(): ?array
    {
        // If no oidc aspect exists then the user did not use
        // oauth to authenticate himself
        if (!isset($this->aspect)) {
            return null;
        }

        $this->logger->debug(implode("\n\t", [
            "Found an OIDC aspect:",
            "is authenticated: " . ($this->aspect->isAuthenticated() ? 'YES' : 'NO'),
            "resource id: " . $this->aspect->getResourceId()
        ]));

        // Check if the authentication process took place and ended successfully.
        // This does not mean yet that the user will be accepted by Typo3!
        // However, the oidc aspect reports that the user is not authenticated
        // if the provider used is not enabled for this application type
        if (!$this->aspect->isAuthenticated()) {
            $this->logger->debug('user has not been authenticated.', [
                'error' => $this->aspect->get('error'),
                'details' => $this->aspect->get('error_description')
            ]);
            // Return a fake user object, so that the authentication will be rejected
            return $this->authFailedUser();
        }

        $resource = $this->aspect->getResource();
        $this->providerId = $resource->getProviderId();

        $this->logger->debug("Converting a resource to a typo3 user", $resource->getClaims());
        $user = $this->convertResourceOwner($resource);

        // Dispatch an event containing the user with his access token if auth was successful,
        // so other extensions can use them to make further requests to an API
        // provided by the authentication server
        if ($user) {
            $this->logger->debug("Created a typo3 user", $user);
            $event = new AuthenticationGetUserEvent($user);
            GeneralUtility::makeInstance(EventDispatcherInterface::class)->dispatch($event);
            $user = $event->getUser();
            return $user;
        } else {
            return $this->authFailedUser();
        }
    }


    /**
     * Authenticates a user
     *
     * @param array $user
     * @return int
     */
    public function authUser(array $user): int|bool
    {
        if (!isset($this->aspect)) {
            // The user has not used an OpenID Connect provider for authentication
            return AuthenticationStatus::FAILURE_CONTINUE;
        }

        $userData = [
            'uid' => $user['uid'],
            'username' => $user['username'],
            'provider' => $this->aspect->getProviderName()
        ];

        if (empty($user['oidc'])) {
            $this->logger->info('The user has not been authenticated', $userData);
            return AuthenticationStatus::FAILURE_BREAK;
        }

        $this->logger->info('The user has been authenticated', $userData);
        return AuthenticationStatus::SUCCESS_BREAK;
    }

    /**
     * Faked data in case the oauth authentication has failed.
     *
     * We have to pass some data, so that the authentication process does not
     * try another way to authenticate the user.
     *
     * @return array
     */
    private function authFailedUser(): array
    {
        return [
            'uid' => -1,
            'username' => 'oidc_failed',
            'password' => '********'
        ];
    }

    /**
     * Converts a resource owner into a TYPO3 user.
     *
     * This method searches for an existing oidc and typo3 user that matches
     * the resource owner and updates the user groups. Special cases:
     * - if no typo3 user exists, then one is created unless told otherwise
     *   (extension setting xxUserMustExist)
     * - if the typo3 user is deleted or disabled, then it is undeleted/re-enabled
     *   unless told otherwise (extension settings xxReEnableUsers and xxUndeleteUsers)
     * - if the oidc user is deleted or disabled, then no user is returned
     *
     * A newly created typo3 user is assigned to the default groups (extension setting
     * xxDefaultGroups) and those with a pattern configured for the current provider
     * and matching the roles for the resource owner. If a user already exists,
     * the currently assigned groups are not affected.
     *
     * @param UserResource $resource
     * @return array|null
     * @throws DbException
     */
    protected function convertResourceOwner(UserResource $resource): ?array
    {
        $storage = $this->aspect->get('storage', '');
        $canCreateUser = $this->aspect->getAuthFeatures(Constants::AUTH_CREATE_USER);
        $canUnlockUser = $this->aspect->getAuthFeatures(Constants::AUTH_UNLOCK_USER);

        // Fetch the matching user and check its status
        // - $row is empty if no oidc user exists
        // - $row[uid] is null if no matching typo3 user exists
        $row = $this->findOidcUser($resource, $storage);
        // A disabled oidc user is rejected immediately
        $oidcDeleted = $row['oidcDeleted'] ?? 0;
        if (($row['oidcDisable'] ?? 0) && !$oidcDeleted) {
            $this->logger->info('Access denied: the oidc account is disabled for the user', [
                'providerId' => $this->providerId,
                'user' => $row,
                'user_type' => $this->loginType
            ]);
            $this->aspect->setError('account_locked', 'the account is blocked');
            return null;
        }

        /** @var $hasDeletedLocalUser - a deleted typo3 user has been found */
        $hasDeletedLocalUser = !$oidcDeleted && ($row['deleted'] ?? 0);
        /** @var $hasLocalUser - a non-deleted typo3 user has been found */
        $hasLocalUser = !$oidcDeleted && !$hasDeletedLocalUser && ($row['uid'] ?? 0);

        if ($hasLocalUser) {
            $this->logger->debug('found a matching local user', [
                'username' => $row['username'],
                'uid' => $row['uid']
            ]);
        } elseif(($shortname=$resource->get('shortname'))) {
            // Find a matching local user if
            // 1. there is no local user associated with the oidc resource, or
            // 2. the local user associated with the oidc resource is deleted.
            // We update the data in the first case or if the matching user
            // is not deleted
            $this->logger->debug('searching for a local user by their shortname', ['shortname' => $shortname]);
            $match = $this->findMatchingUser('username', $shortname, $storage);
            if ($match && !($match['deleted'] && $hasDeletedLocalUser)) {
                $this->logger->debug('found a matching local user by their shortname', [
                    'username' => $match['username'],
                    'uid' => $match['uid'],
                    'pid' => $match['pid']
                ]);
                $row = array_merge($row, $match);
                $hasDeletedLocalUser = (bool)$row['deleted'];
                $hasLocalUser = !$hasDeletedLocalUser;
            } else {
                $this->logger->debug('no matching user has been found', ['claims' => $resource->getClaims()]);
            }
        } elseif (($email=$resource->get('email'))) {
            // Find a matching local user if
            // 1. there is no local user associated with the oidc resource, or
            // 2. the local user associated with the oidc resource is deleted.
            // We update the data in the first case or if the matching user
            // is not deleted
            $this->logger->debug('searching for a local user by their email', ['email' => $email]);
            $match = $this->findMatchingUser('email', $email, $storage);
            if ($match && !($match['deleted'] && $hasDeletedLocalUser)) {
                $this->logger->debug('found a matching local user by their email', [
                    'username' => $match['username'],
                    'email' => $email,
                    'uid' => $match['uid'],
                    'pid' => $match['pid']
                ]);
                $row = array_merge($row, $match);
                $hasDeletedLocalUser = (bool)$row['deleted'];
                $hasLocalUser = !$hasDeletedLocalUser;
            } else {
                $this->logger->debug('no matching user has been found', ['claims' => $resource->getClaims()]);
            }
        } else {
            $this->logger->debug('no matching user has been found', ['claims' => $resource->getClaims()]);
        }

        if ($hasLocalUser) {
            // Enable the user if necessary and allowed to do so
            if ($row['disable']) {
                if ($canUnlockUser) {
                    $this->updateTypo3User($row['uid'], ['disable' => 0]);
                } else {
                    $this->logger->info('Access denied: the local user is disabled', [
                        'user' => $row,
                        'user_type' => $this->loginType
                    ]);
                    $this->aspect->setError('blocked_user', 'the access is blocked for the user ' . $row['username']);
                    return null;
                }
            }
        } elseif (!$canCreateUser) {
            // No typo3 user and it should not be created on-the-fly
            $this->logger->info('Access denied: no local user exists', [
                'providerId' => $this->providerId,
                'claims' => $resource->getClaims(),
                'user_type' => $this->loginType,
                'pages' => $storage
            ]);
            $this->aspect->setError('unknown_user', 'the user is not recognized');
            return null;

        } elseif ($hasDeletedLocalUser && $this->authConfig->userRecycle()) {
            // Recycle the deleted user and make sure that it is enabled
            $this->updateTypo3User($row['uid'], ['deleted' => 0, 'disable' => 0]);

        } else {
            // Create a new typo3 user and set 'username' to resource id if not provided.
            // Merge with $row, because the array may already contain oidc data.
            $pageId = GeneralUtility::intExplode(',', $storage, true)[0] ?? 0;
            $row = array_merge($row, $this->createTypo3User($resource, $pageId));
            $this->logger->info('a local user has been created for the oidc login', [
                'username' => $row['username'],
                'uid' => $row['uid'],
                'pid' => $pageId
            ]);
        }

        // Create an oidc user if none exists so far.
        // Otherwise, update the record if it does not point to the typo3 user
        // or if it is marked as deleted.
        // This can happen if the typo3 user matching this oidc user has been
        // removed previously, but the oidc user was not.
        if (!($row['oidcUid'] ?? 0)) {
            $row['oidc'] = $this->createOidcUser($row, $resource);
        } else {
            $this->updateAndEnableUserLink($row['oidcUid'], $row, $resource);
            $row['oidc'] = $row['oidcUid'];
        }
        // Return the user record with oidc account id - the existence
        // of this property is checked by authUser()
        return array_diff_key($row, array_flip(
            ['oidcUid', 'oidcUser', 'oidcDisable', 'oidcDeleted']
        ));
    }

    /**
     * Fetches user data matching the resource owner.
     *
     * The data is fetched from the oidc user table joined with the typo3
     * user table. In a healthy database an oidc user is always related
     * to a typo3 user. When this is not the case, only oidc user data
     * is returned.
     *
     * @param UserResource $resource
     * @param string $pageIds
     * @return array
     * @throws DbException|DBALException
     */
    protected function findOidcUser(UserResource $resource, string $pageIds = ''): array
    {
        // Fetch an oidc user with a matching Typo3 user when one exists
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->oidcUserTable);
        $queryBuilder->getRestrictions()->removeAll();
        $expr = $queryBuilder->expr();
        $query = $queryBuilder
            ->select(
                'user.*',
                'oidc.disable AS oidcDisable',
                'oidc.deleted AS oidcDeleted',
                'oidc.user AS oidcUser',
                'oidc.uid AS oidcUid'
            )->from($this->oidcUserTable, 'oidc')
            ->leftJoin(
                'oidc', $this->pObj->user_table, 'user',
                $expr->eq('oidc.user', 'user.uid')
            )->where(
                $expr->eq('oidc.sub', $queryBuilder->quote($resource->getId())),
                $expr->eq('oidc.provider', $resource->getProviderId())
            );
        if ($pageIds) $query->andWhere($expr->or(
            $expr->isNull('user.pid'),
            $expr->in('user.pid', $pageIds)
        ));
        return $query->executeQuery()->fetchAssociative() ?: [];
    }

    /**
     * Returns a typo3 user with a given email address.
     *
     * The method checks if there is more than one non-deleted user
     * with the given email address. If so, no user is returned to
     * avoid mismatches. If all matching users are deleted, the latest
     * one is picked for a possible recycle.
     *
     * @param string $field
     * @param string $value
     * @param string $pageIds
     * @return array
     * @throws DBALException|DbException
     */
    protected function findMatchingUser(string $field, string $value, string $pageIds = ''): array
    {
        // Fetch a matching Typo3 user
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->pObj->user_table);
        $queryBuilder->getRestrictions()->removeAll();
        $expr = $queryBuilder->expr();
        $users = $queryBuilder
            ->select('*')
            ->from($this->pObj->user_table)
            ->where($expr->eq($field, $queryBuilder->quote($value)));
        if ($pageIds) $users->andWhere($expr->in('pid', $pageIds));
        $users = $users
            ->orderBy('deleted', 'ASC')
            ->addOrderBy('tstamp', 'DESC')
            ->setMaxResults(2)
            ->executeQuery()
            ->fetchAllAssociative();

        return (count($users) == 2 && !$users[1]['deleted'])
            ? []                    // more than one matching local user
            : ($users[0] ?? []);    // there still might be no matching user
    }

    /**
     * Creates a typo3 user in the database for the provided resource owner
     * and returns the full record created for the user.
     *
     * @param UserResource $resource
     * @param int $pageId
     * @return array
     * @throws DbException
     */
    protected function createTypo3User(UserResource $resource, int $pageId): array
    {
        $defaultGroups = $this->aspect->getProviderConfig()[$this->loginType . '_groups'] ?? '';
        $userGroups = array_unique(array_merge(
            GeneralUtility::intExplode(',', $defaultGroups, true),
            $this->getMatchingGroups($resource)
        ));
        sort($userGroups, SORT_NUMERIC);

        $userData = array_merge(
            ['username' => $resource->getId()],
            $this->getMappedProperties($resource),
            [
                $this->pObj->usergroup_column => implode(',', $userGroups),
                $this->pObj->userident_column => $this->generatePassword(),
                'pid' => $pageId,
                'tstamp' => $GLOBALS['EXEC_TIME'],
                'crdate' => $GLOBALS['EXEC_TIME']
            ]
        );

        $connection = $this->connectionPool->getConnectionForTable($this->pObj->user_table);
        $connection->insert($this->pObj->user_table, $userData);

        // fetch the full record
        return $connection->select(
            ['*'],
            $this->pObj->user_table,
            ['uid' => $connection->lastInsertId()]
        )->fetchAssociative();
    }

    /**
     * Returns the array with default mapping rules.
     *
     * @return array
     */
    protected function getDefaultMappingRules(): array
    {
        $resourceOwnerIdClaim = $this->aspect->getProviderConfig()['id_claim'] ?: 'sub';
        return [
            'username'   => ['<email>', '<sub>', "<$resourceOwnerIdClaim>"],
            'name'       => ['<given_name> <family_name>'],
            'first_name' => ['<given_name>'],
            'last_name'  => ['<family_name>'],
            'email'      => ['<email>']
        ];
    }

    /**
     * Computes properties using the provided claims.
     *
     * This method evaluates for each mapped property the associated templates
     * by substituting claim references (e.g. <cn>) with their corresponding values.
     * If a claim is not provided, then the empty value is taken. The first
     * nontrivial value is taken as the value of the property.
     *
     * @param UserResource $resource
     * @return array
     */
    public function getMappedProperties(UserResource $resource): array
    {
        $rules = \json_decode(
            $this->aspect->getProviderConfig()['mapping'] ?? '',
            true
        ) ?: $this->getDefaultMappingRules();

        $result = [];
        foreach ($rules as $property => $templates) {
            foreach ($templates as $template) {
                $value = $this->evaluateTemplate($template, $resource->getClaims());
                if ($value) {
                    $result[$property] = $value;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Replaces all claim references (e.g. <cn>) in the template string with their
     * corresponding values from the provided array. If a claim is not provided,
     * then the empty value is taken.
     *
     * @param string $template  The template string to be evaluated
     * @param array $claims     The list of claims for substitutions
     * @return string    The evaluated template
     */
    protected function evaluateTemplate(string $template, array $claims): string
    {
        return preg_replace_callback(
            '/<(.+?)>/',
            function ($matches) use($claims) { return $claims[$matches[1]] ?? ''; },
            $template
        );
    }

    /**
     * Creates an oidc account record from a user resource
     *
     * @param array $user
     * @param UserResource $resource
     * @return int
     */
    protected function createOidcUser(array $user, UserResource $resource): int
    {
        $connection = $this->connectionPool->getConnectionForTable($this->oidcUserTable);
        $connection->insert(
            $this->oidcUserTable,
            [
                'pid' => $user['pid'],
                'sub' => $resource->getId(),
                'provider' => $resource->getProviderId(),
                'user' => $user['uid'],
                'email' => $resource->get('email_verified') === false ? null : $resource->get('email'),
                'last_auth' => $GLOBALS['EXEC_TIME'],
                'tstamp' => $GLOBALS['EXEC_TIME'],
                'crdate' => $GLOBALS['EXEC_TIME']
            ],
            [
                Connection::PARAM_INT,
                Connection::PARAM_STR,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_STR,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT
            ]
        );
        return (int)$connection->lastInsertId();
    }

    /**
     * Updates the user record. No value checks is made: data must be sanitized.
     *
     * @param int $uid
     * @param array $data
     * @return void
     */
    protected function updateTypo3User(int $uid, array $data): void
    {
        $connection = $this->connectionPool->getConnectionForTable($this->pObj->user_table);
        $connection->update(
            $this->pObj->user_table,
            array_merge($data, [
                'tstamp' => $GLOBALS['EXEC_TIME']
            ]),
            ['uid' => $uid]
        );
    }

    /**
     * Updates an oidc user record and set `disable` and `deleted` fields to 0.
     *
     * @param int $oidcUserId
     * @param array $typo3user
     * @param UserResource $resource
     * @return void
     */
    protected function updateAndEnableUserLink(int $oidcUserId, array $typo3user, UserResource $resource): void
    {
        $connection = $this->connectionPool->getConnectionForTable($this->oidcUserTable);
        $connection->update(
            $this->oidcUserTable,
            [
                'user' => $typo3user['uid'],
                'pid' => $typo3user['pid'],
                'deleted' => 0,
                'disable' => 0,
                'tstamp' => $GLOBALS['EXEC_TIME'],
                'email' => $resource->get('email_verified') === false ? null : $resource->get('email'),
                'last_auth' => $GLOBALS['EXEC_TIME'],
            ],
            ['uid' => $oidcUserId],
            [
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_STR,
                Connection::PARAM_INT
            ]
        );
    }

    /**
     * Returns an array of user groups matching the resource owner.
     *
     * The groups are found by evaluating group patterns configured
     * for the current oidc provider against the list of roles of
     * the resource owner.
     *
     * @param UserResource $resource
     * @return array
     * @throws DbException
     */
    protected function getMatchingGroups(UserResource $resource): array
    {
        // check patterns against roles
        // patterns are in tx_multioidc_xegroup

        // Get roles of the resource owner and prepare a testing string:
        // - each role surrounded by commas, including the first and the last one
        // - no whitespaces
        $roles = $resource->get('Roles');
        if (!$roles) return []; // nothing to process
        $roles = ',' . implode(',',
                is_array($roles) ? array_filter(array_map('trim', $roles))
                    : GeneralUtility::trimExplode(',', $roles, true)
            ) . ',';

        // Fetch all patterns for this provider
        $connection = $this->connectionPool->getConnectionForTable($this->oidcGroupTable);
        $patterns = $connection->select(
            ['group', 'pattern'],
            $this->oidcGroupTable,
            ['provider' => $this->providerId]
        )->fetchAllAssociative();

        // Match patterns against roles
        // A pattern is a |-separated list of alternative roles name,
        // where '*' stands for any sequence of characters.
        $groups = [];
        foreach($patterns as $patternData) {
            $pattern = '/,(' . implode('|', array_map(
                function ($re) {
                    return str_replace('\\*', '[^,]*', preg_quote($re, '/'));
                },
                GeneralUtility::trimExplode('|', $patternData['pattern'], true)
            )) . '),/i';
            if (preg_match($pattern, $roles)) {
                $groups[] = $patternData['group'];
            }
        }
        return $groups;
    }

    /**
     * Filters groups from the list that are associated with patterns
     * configured for the current provider.
     *
     * @param array $groups
     * @return array
     * @throws DbException
     */
    protected function filterOidcGroups(array $groups): array
    {
        if (empty($groups)) return $groups;

        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->oidcGroupTable);
        return $queryBuilder
            ->select('group')
            ->from($this->oidcGroupTable)
            ->where(
                $queryBuilder->expr()->in('group', $groups),
                $queryBuilder->expr()->eq('provider', $this->providerId)
            )
            ->executeQuery()
            ->fetchFirstColumn();
    }

    /**
     * A helper function that generates a random user password.
     *
     * @return string
     */
    protected function generatePassword(): string
    {
        $password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$'), 0, 20);

        $passwordHashFactory = GeneralUtility::makeInstance(PasswordHashFactory::class);
        try {
            $objInstanceSaltedPW = $passwordHashFactory->getDefaultHashInstance($this->authInfo['loginType']);
            return $objInstanceSaltedPW->getHashedPassword($password);
        } catch (InvalidPasswordHashException $e) {
            $this->logger->error("failed to generate a password hash factory - unhashed random password is saved: " . $e->getMessage());
            return $password;
        }
    }

}
