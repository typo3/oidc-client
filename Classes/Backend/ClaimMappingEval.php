<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Backend;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A class translating data saved in tx_multioidcclient_provider.mapping
 * for displaying in the backend editor.
 *
 * The data is stored as an JSON object, so that no parsing is required when it is
 * accessed by the PHP code. In the editor, however, it is a text with each line
 * having the format
 *   <property> = <template1> // <template2> // ...
 * Lines starting with '#' are treated as comments and are preserved. Comments are
 * stored with keys '#comment:<number>'.
 */
class ClaimMappingEval
{
    const COMMENT_TOKEN = '#';
    const COMMENT_KEY = '#comment:';

    /**
     * Converts a text from the editor into a JSON object or null to be saved in the database
     *
     * @param string $value
     * @param string $is_in
     * @param bool $set
     * @return string
     */
    public function evaluateFieldValue(string $value, string $is_in, bool &$set): ?string
    {
        if (!$value) {
            return null;
        }
        $rules = [];
        $comments = 0;
        foreach (GeneralUtility::trimExplode("\n", $value, true) as $line) {
            // Test if a comment
            if ($line[0] === self::COMMENT_TOKEN) {
                $rules[self::COMMENT_KEY . $comments] = substr($line, 1);
            } else {
                $parts = explode('=', $line, 2);
                // the property is already trimmed from the left side
                $property = rtrim($parts[0] ?? '');
                if ($property) {
                    $rules[$property] = GeneralUtility::trimExplode(' // ', $parts[1] ?? '', true);
                }
            }
        }
        return $rules ? \json_encode($rules) : null;
    }

    /**
     * Converts the JSON object or null stored in the database into a text displayed
     * in the backend editor. In case the conversion fails, the raw data is returned
     * with each line converted to a comment.
     *
     * @param array $parameters
     * @return string
     */
    public function deevaluateFieldValue(array $parameters): string
    {
        if ($parameters['value'] ?? null) {
            $decoded = \json_decode($parameters['value'], true);
            if (is_array($decoded)) {
                return implode("\n", array_map(
                    function (string $key, $value) {
                        return str_starts_with($key, self::COMMENT_KEY)
                            ? self::COMMENT_TOKEN . $value
                            : $key . ' = ' . implode(' // ', $value);
                    },
                    array_keys($decoded),
                    array_values($decoded)
                ));
            } else {
                return "### Parsing error - raw value is displayed below ###\n# "
                    . str_replace("\n", "\n# ", $parameters['value']);
            }
        }
        return '';
    }
}
