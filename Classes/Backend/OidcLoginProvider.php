<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Backend;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Domain\Repository\ProviderLabelRepository;
use IMATHUZH\OidcClient\Service\Configuration;
use IMATHUZH\OidcClient\Utility\Constants;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Backend\Controller\LoginController;
use TYPO3\CMS\Backend\LoginProvider\LoginProviderInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * The OIDC login provider for the backend login page.
 *
 * This provider displays buttons for configured oidc providers
 * that redirect to the authentication pages of the providers.
 */
class OidcLoginProvider implements LoginProviderInterface
{
    protected ?OidcAspect $aspect = null;

    public function __construct(
        protected ProviderLabelRepository $providerRepository,
        protected Context $context
    ) { }

    /**
     * @inheritDoc
     */
    public function render(StandaloneView $view, PageRenderer $pageRenderer, LoginController $loginController)
    {
        if ($this->context->hasAspect(Constants::ASPECT)) {
            $this->aspect = $this->context->getAspect(Constants::ASPECT);
        }

        $view->setTemplatePathAndFilename(
            GeneralUtility::getFileAbsFileName(
                'EXT:'.Constants::EXT_KEY.'/Resources/Private/Templates/Backend/LoginProvider.html'
            )
        );
        $view->assign('providers', $this->providerRepository->findForBackend());

        $settings = Configuration::getInstance()->backend();
        $view->assign('oidc_redirect_url', $settings->redirectUrl());

        if (($error = $this->getAuthError())) {
            $view->assign('oidcError', $error);
        }
    }

    /**
     * A helper function that returns true is there is a logged in BE user
     * and false otherwise.
     *
     * @return bool
     */
    private function isBackendUserLoggedIn(): bool
    {
        try {
            return $this->context->getPropertyFromAspect('backend.user', 'isLoggedIn');
        } catch (AspectNotFoundException $e) {
            // No aspect -> no logged-in user
            return false;
        }
    }

    /**
     * Returns the authentication error if any
     */
    private function getAuthError(): ?string
    {
        // No error if
        // - oauth authentication did not take place, or
        // - the user is authenticated and logged-in
        if (!$this->aspect || (
            $this->aspect->isAuthenticated()
            && $this->isBackendUserLoggedIn()
        )) {
            return null;
        }

        /**
         * @todo Avoid duplication by moving this code into OidcAspect
         */
        $error = $this->aspect->get('error_description', '')
              ?: $this->aspect->get('error', '');
        // Decode if a JSON object
        if ($error && $error[0] === '{') {
            $data = \json_decode($error);
            if ($data) {
                $error = $data->error_description ?: $data->error ?: '';
            } else {
                $error = '';
            }
        }

        // Return the error
        /** @todo Use language service instead a hard-coded string */
        $provider = $this->aspect->getProviderTitle();
        return "The authentication against $provider has failed"
            . ($error ? ": $error." : '.');
    }
}
