<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Backend;

use IMATHUZH\OidcClient\Domain\Model\ProviderLabel;
use IMATHUZH\OidcClient\Domain\Repository\ProviderLabelRepository;
use IMATHUZH\OidcClient\Utility\Constants;
use IMATHUZH\OidcClient\Utility\StaticLoggerTrait;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * User functions for formatting and processing records with oidc user identifiers
 * group patterns in backend tables.
 */
class TCA
{
    use StaticLoggerTrait;

    static public function debugFlex(&$params): void
    {
        $logger = self::getLogger();
        $logger->debug(var_export($params, true));
    }

    static private function oidcRecordTitle(
        array $row,
        string $labelField,
        string $parentTable,
        string $parentField,
        string $foreignField,
        string $appType
    ): string
    {
        $label = $row[$labelField];
        $parentId = $row[$parentField] ?? 0;
        if (is_array($parentId)) $parentId = $parentId[0];
        if ($parentId) {
            $parentRow = BackendUtility::getRecord(
                $parentTable, $parentId, $foreignField
            ) ?? [];
            $label = $parentRow[$foreignField] . ': ' . $label;
        }
        if ($row['provider'] ?? 0) {
            // $row['provider'] is
            // - an array on record edit page
            // - a string on oidc tab of a user edit page
            $provider = BackendUtility::getRecord(
                Constants::TABLE_OIDC_CONFIG,
                is_array($row['provider']) ? $row['provider'][0] : $row['provider'],
                "title,deleted,$appType",
                '',
                false
            ) ?? [];
            $enabled = ($provider[$appType] ?? 0) && Constants::AUTH_ENABLED;
            $providerName = $provider['title'] ?? 'unknown provider';
            if ($provider['deleted'] ?? false) {
                $providerName .= ' [deleted]';
            } elseif (!$enabled) {
                $providerName .= ' [disabled]';
            }
            $label .= " ($providerName)";
        }
        return $label;
    }

    /**
     * The title of the record with an oidc identifier of a frontend user
     * @param array $params
     * @return void
     */
    static public function oidcFeUserRecordTitle(array &$params)
    {
        $params['title'] = self::oidcRecordTitle($params['row'], 'sub', 'fe_users', 'user', 'username', 'frontend');
    }

    /**
     * The title of the record with an oidc identifier of a backend user
     * @param array $params
     * @return void
     */
    static public function oidcBeUserRecordTitle(array &$params)
    {
        $params['title'] = self::oidcRecordTitle($params['row'], 'sub', 'be_users', 'user', 'username', 'backend');
    }

    /**
     * The title of the record with an oidc pattern mapping a frontend user group
     * @param array $params
     * @return void
     */
    static public function oidcFeGroupRecordTitle(array &$params)
    {
        $params['title'] = self::oidcRecordTitle($params['row'], 'pattern', 'fe_groups', 'group', 'title', 'frontend');
    }

    /**
     * The title of the record with an oidc pattern mapping a backend user group
     * @param array $params
     * @return void
     */
    static public function oidcBeGroupRecordTitle(array &$params)
    {
        $params['title'] = self::oidcRecordTitle($params['row'], 'pattern', 'be_groups', 'group', 'title', 'backend');
    }

    static public function frontendProviders(&$params): void
    {
        self::getLogger()->debug("TCA::frontendProviders():\n\t" . implode("\n\t",
            array_map(
                fn($item) => implode(', ', $item),
                $params['items']
            )
        ));
        /** @var ProviderLabel[] $providers */
        $providers = GeneralUtility::makeInstance(ProviderLabelRepository::class)->findAll()->toArray();
        $providers = array_combine(
            array_map(fn($p) => $p->getUid(), $providers),
            $providers
        );
        foreach ($params['items'] as &$provider) {
            $providerId = $provider[1];
            if ($providers[$providerId] && !$providers[$providerId]->isEnabledForFrontend()) {
                $provider[0] .= " [disabled]";
            }
        }
        self::getLogger()->debug("TCA::frontendProviders():\n\t" . implode("\n\t",
                array_map(
                    fn($item) => implode(', ', $item),
                    $params['items']
                )
            ));
    }
}
