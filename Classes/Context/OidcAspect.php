<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Context;

use IMATHUZH\OidcClient\OAuth2\UserResource;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Core\Context\AspectInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectPropertyNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * An aspect with the result of an oauth authentication.
 */
class OidcAspect implements AspectInterface
{
    /**
     * @var UserResource the resource of the authenticated user (can be unset)
     */
    protected UserResource $resource;

    /**
     * @var string an authentication error
     */
    protected string $error = '';

    /**
     * @var string a description of the authentication error
     */
    protected string $errorDescription = '';

    static public function createError(string $error, string $details = ''): self
    {
        $aspect = new self();
        $aspect->error = $error;
        $aspect->errorDescription = $details;

        GeneralUtility::makeInstance(Context::class)->setAspect(Constants::ASPECT, $aspect);

        return $aspect;
    }

    public function __construct(
        protected array $providerConfig = [],
        protected array $parameters = [],
        protected int $authFeatures = 0
    ) { }

    /**
     * Returns a value stored in this aspect. Supported values:
     * - `authenticated` (bool)
     * - `provider_config` (array)
     * - `provider_id` (int)
     * - `provider_name` (string)
     * - `resource_id` (string)
     * - `resource` (UserResource)
     * - `error` (string)
     * - `error_description` (string)
     * - any value passed as a parameter to the authentication request
     *
     * If the value is not found, then the default value is returned
     * when specified or an exception is thrown otherwise.
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed
     * @throws AspectPropertyNotFoundException
     */
    public function get(string $name, mixed $default=null)
    {
        switch ($name) {
            case 'authenticated': return $this->isAuthenticated();
            case 'provider_config': return $this->providerConfig;
            case 'provider_id': return $this->getProviderId();
            case 'provider_name': return $this->getProviderName();
            case 'resource_id': return $this->getResourceId();
            case 'resource': return $this->resource;
            case 'error': return $this->error;
            case 'error_description': return $this->errorDescription;
            case 'features': return $this->authFeatures;
            default:
                if (isset($this->parameters[$name])) {
                    return $this->parameters[$name];
                } elseif (is_null($default)) {
                    throw new AspectPropertyNotFoundException('Property "'.$name.'" not found in aspect "'.__CLASS__.'".', 1707781422);
                } else {
                    return $default;
                }
        }
    }

    public function isAuthenticated(): bool
    {
        return isset($this->resource);
    }

    public function getResource(): UserResource
    {
        return $this->resource;
    }

    public function getProviderId(): int
    {
        return $this->providerConfig['uid'] ?? 0;
    }

    public function getProviderName(): string
    {
        return $this->providerConfig['name'] ?? '';
    }

    public function getProviderTitle(): string
    {
        return $this->providerConfig['title'] ?? '';
    }

    public function getProviderConfig(): array
    {
        return $this->providerConfig;
    }

    public function getResourceId(): ?string
    {
        return isset($this->resource) ? $this->resource->getId() : null;
    }

    public function getAuthFeatures(int $mask = 0xff): int
    {
        return $this->authFeatures & $mask;
    }

    public function setAuthFeatures(int $features): void
    {
        $this->authFeatures = $features;
    }

    public function toggleAuthFeatures(int $features, ?bool $value = null): int
    {
        if ($value === null) {
            $this->authFeatures ^= $features;
        } elseif ($value) {
            $this->authFeatures |= $features;
        } else {
            $this->authFeatures &= ~$features;
        }
        return $this->authFeatures;
    }

    public function setError(string $error, string $description = ''): void
    {
        $this->error = $error;
        $this->errorDescription = $description;
    }

    public function setResource(UserResource $resource): void
    {
        $this->resource = $resource;
    }
}
