<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Controller;

use IMATHUZH\OidcClient\Domain\Model\ProviderCard;
use IMATHUZH\OidcClient\Domain\Repository\ProviderCardRepository;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * The backend controller for OIDC providers.
 */
class BackendController extends ActionController
{
    protected ModuleTemplateFactory $moduleTemplateFactory;

    /**
     * @var ProviderCardRepository   A repository of records with data to build provider cards
     */
    protected ProviderCardRepository $providerRepository;

    public function __construct(
        ModuleTemplateFactory $moduleTemplateFactory,
        StandaloneView $view,
        ProviderCardRepository $providerRepository
    ) {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->providerRepository = $providerRepository;
        $this->view = $view;
    }

    /**
     * The action that updates a provider configuration (authentication features
     * and ordering), triggered from the main view.
     *
     * @param int $uid
     * @param int $target
     * @param array $frontend
     * @param array $backend
     * @param bool $mfa
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function updateAction(int $uid, int $target=0, array $frontend=[], array $backend=[], bool $mfa=false): ResponseInterface
    {
        if ($target) $this->providerRepository->reorder($uid, $target);
        $this->providerRepository->updateConfiguration(
            $uid, self::union($frontend), self::union($backend), $mfa
        );
        return $this->redirect('overview');
    }

    /**
     * Renders the main page with a list of cards for all providers.
     *
     * @return ResponseInterface
     */
    public function overviewAction(): ResponseInterface
    {
        $this->view->assign('providers', array_map(
            fn (ProviderCard $card) => $card->unpack(),
            $this->providerRepository->findAll()->toArray()
        ));
        $template = $this->moduleTemplateFactory->create($this->request);
        $template->setContent($this->view->render());
        return $this->htmlResponse($template->renderContent());
    }

    /**
     * A bitwise union of integer values
     *
     * @param array $data
     * @return int
     */
    private static function union(array $data): int
    {
        return array_reduce($data, fn($current, $value) => $current | $value, 0);
    }
}
