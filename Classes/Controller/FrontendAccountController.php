<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Controller;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Domain\Model\ProviderLabel;
use IMATHUZH\OidcClient\Domain\Repository\FrontendAccountRepository;
use IMATHUZH\OidcClient\Domain\Repository\ProviderLabelRepository;
use IMATHUZH\OidcClient\Event\AccountLinkedEvent;
use IMATHUZH\OidcClient\Event\AccountUnlinkedEvent;
use IMATHUZH\OidcClient\Event\AccountUpdatedEvent;
use IMATHUZH\OidcClient\Event\BeforeLinkAccountEvent;
use IMATHUZH\OidcClient\OAuth2\UserResource;
use IMATHUZH\OidcClient\Service\OAuthService;
use IMATHUZH\OidcClient\Service\UserStorage;
use IMATHUZH\OidcClient\Utility\Constants;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Authentication\AbstractUserAuthentication;
use TYPO3\CMS\Core\Context\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * The controller for the manager of oidc accounts linked to a frontend user.
 */
class FrontendAccountController extends ActionController implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $messages = [
        'no_user' => 'warning',
        'auth_failure' => 'error',
        'update_ok' => 'notice',
        'update_locked' => 'error',
        'update_mismatch' => 'warning',
        'update_no_account' => 'error',
        'update_error' => 'error',
        'link_ok' => 'notice',
        'account_exists' => 'error',
        'link_error' => 'error',
        'unlink_ok' => 'notice',
        'unlink_failed' => 'warning',
        'unlink_locked' => 'error',
        'unlink_error' => 'error',
        'no_user_error' => 'warning',
        'user_changed' => 'warning'
    ];

    protected AbstractUserAuthentication $user;

    protected ?array $loggedInUser = null;

    public function __construct(
        protected FrontendAccountRepository $oidcAccountRepository,
        protected ProviderLabelRepository $providerRepository,
        protected OAuthService $oauthService
    ) { }

    /**
     * Initializes data related to the currently logged-in user
     *
     * @return void
     */
    public function initializeAction()
    {
        $this->user = $this->getCurrentUser();
        if ($this->user->user['uid'] ?? 0) {
            $this->loggedInUser = $this->user->user;
        }
    }

    /**
     * Displays a list of oidc accounts linked to the current user
     */
    public function overviewAction(): ResponseInterface
    {
        $this->view->setTemplateRootPaths(['EXT:oidc_client/Resources/Private/Templates/FrontendAccount']);
        if ($this->loggedInUser) {

            // Prepare providers to match against accounts
            $enabledProviders = GeneralUtility::intExplode(',', $this->settings['provider'], true);
            $providers = $this->providerRepository->findAll()->toArray();
            $providerData = array_combine(
                array_map(fn(ProviderLabel $provider) => $provider->getUid(), $providers),
                array_map(fn(ProviderLabel $provider) => [
                    $provider, in_array($provider->getUid(), $enabledProviders)
                ], $providers)
            );

            // Get accounts with a matching provider.
            $accounts = array_filter(
                $this->oidcAccountRepository->findForUser($this->loggedInUser['uid']),
                fn ($account) => isset($providerData[$account['provider']])
            );
            // Precompute properties of user accounts
            foreach ($accounts as &$account) {
                if (isset($providerData[$account['provider']])) {
                    [$provider, $isEnabled] = $providerData[$account['provider']];

                    // Account status
                    if ($account['disable']) {
                        $account['status'] = 'locked';
                        $account['updatable'] = $isEnabled && $this->settings['enableOidcAccount'];
                    } elseif ($isEnabled) {
                        $account['status'] = 'active';
                        $account['updatable'] = true;
                    } else {
                        $account['status'] = 'disabled';
                        $account['updatable'] = false;
                    }
                    // Update the provider property and add secured settings
                    $account['provider'] = $provider;
                    $account['providerEnabled'] = $isEnabled;
                    $account['updateSettings'] = $this->oauthService->encodeSettings([
                        'target_user' => $this->loggedInUser['uid'],
                        'account' => $account['uid']
                    ]);
                }
            }
            // Filter disabled providers
            $providers = array_filter(
                $providers,
                fn (ProviderLabel $provider) => $provider->isEnabledForFrontend()
                    && in_array($provider->getUid(), $enabledProviders)
            );

            $this->view->assignMultiple([
                'providers' => $providers,
                'accounts' => $accounts,
                'user' => $this->loggedInUser['uid'],
                'user_email' => $this->loggedInUser['email'],
                'linkSettings' => $this->oauthService->encodeSettings([
                    'target_user' => $this->loggedInUser['uid']
                ])
            ]);
        }
        if (($msg = $this->getActionResult())) {
            $this->view->assign('message', $msg);
        } elseif (!$this->loggedInUser) {
            $this->view->assign('message', $this->getMessage('no_user'));
        }

        return $this->htmlResponse($this->view->render());
    }

    /**
     * Unlinks and removes an oidc account
     */
    public function unlinkAction(int $userId, int $uid, string $provider=''): ResponseInterface
    {
        try {
            // Check that a proper user is logged in
            if ($this->validateLoggedInUser($userId)) {
                // Fetch the account for the AccountUnlinked event
                $account = $this->oidcAccountRepository->find($uid);
                if ($account && $account['disable'] && !$this->settings['enableOidcAccount']) {
                    // Make the account anonymous by setting user=0. This way, if the user
                    // tries to use it account again, it will be blocked.
                    if ($this->oidcAccountRepository->unlink($uid, $userId)) {
                        $this->logger->info('a locked account assigned user=0');
                        $this->storeActionResult('unlink_ok', $provider);
                    } else {
                        $this->logger->info('unlink failed: probably no account exists');
                        $this->storeActionResult('unlink_failed', $provider);
                    }
                } elseif (empty($account) || $this->oidcAccountRepository->delete($uid, $userId)) {
                    // There is no account to be deleted - show as success
                    $this->storeActionResult('unlink_ok', $provider);
                    $this->getUserStorage()->delete($uid);
                    $account && $this->dispatchEvent(new AccountUnlinkedEvent($account));
                } else {
                    $this->logger->info('unlink failed: probably no account exists');
                    $this->storeActionResult('unlink_failed', $provider);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error("failed to unlink the $provider account with uid $uid: " . $e->getMessage());
            $this->storeActionResult('unlink_error', $provider, $e->getCode());
        }
        return $this->redirect('overview');
    }

    /**
     * Updates an existing oidc account
     *
     * @param int $uid    the UID of the oauth account to be updated
     * @return ResponseInterface
     */
    public function updateAction(): ResponseInterface
    {
        $oidc = $this->getOidcAspect();
        if ($this->validateOidc($oidc) && $this->validateLoggedInUser(intval($oidc->get('target_user')))) {
            $resource = $oidc->getResource();
            $provider = $oidc->getProviderTitle();
            if (($accountId = intval($oidc->get('account')))) {
                $this->logger->debug("Updating the oidc account $accountId");
                try {
                    $account = $this->oidcAccountRepository->find($accountId);
                    if (empty($account) || $account['user'] !== $this->loggedInUser['uid']) {
                        // failed: no account
                        $this->logger->error('Update failed: the requested account has not been found');
                        $this->storeActionResult('update_no_account', $provider);
                    } elseif ($account['sub'] !== $resource->getId() || $account['provider'] !== $resource->getProviderId()) {
                        // failed: account mismatch
                        $this->logger->error('Update failed: the requested account does not match');
                        $this->storeActionResult('update_mismatch', $provider);
                    } elseif ($account['disable'] && !$this->settings['enableOidcAccount']) {
                        // account locked
                        $this->logger->error('Update failed: the account is locked');
                        $this->storeActionResult('update_locked', $provider, $account['email']);
                    } elseif ($this->oidcAccountRepository->update($accountId, $resource)) {
                        // update is possible
                        $this->dispatchEvent(new AccountUpdatedEvent($accountId, $resource));
                        $this->storeActionResult('update_ok', $provider);
                    } else {
                        $this->logger->debug("update failed: account mismatch");
                        $this->storeActionResult('update_mismatch', $provider);
                    }
                } catch (\Exception $e) {
                    $this->logger->error('Updating an account has failed: ' . $e->getMessage());
                    $this->storeActionResult('update_error', $provider);
                }
            } else {
                // Missing parameter
                $this->logger->warning("Ignoring update action, because account id is missing");
            }
        }
        return $this->redirect('overview');
    }

    /**
     * Checks if an oidc account can be linked to the current user.
     *
     * This method fires BeforeLinkAccountEvent, allowing 3rd parties
     * to cancel the linking process.
     *
     * @param UserResource $resource
     * @param string $provider
     * @return bool
     */
    protected function canLinkAccount(UserResource $resource, string $provider): bool
    {
        $this->logger->debug("Firing an event to allow 3rd parties to prevent the link");
        $event = new BeforeLinkAccountEvent($resource);
        $this->dispatchEvent($event);
        if ($event->isLinkingCancelled()) {
            $reason = $event->getReason();
            $this->logger->info("account linking has been cancelled: $reason");
            $this->storeActionResult('cannot_link', $provider, $reason);
            return false;
        }
        return true;
    }

    /**
     * Adds or updates an oidc account
     */
    public function linkAction(): ResponseInterface
    {
        $oidc = $this->getOidcAspect();
        if ($this->validateOidc($oidc)) {

            $targetUser = intval($oidc->get('target_user', 0));
            $resource = $oidc->getResource();
            $provider = $oidc->getProviderTitle();

            if ($this->validateLoggedInUser($targetUser) && $this->canLinkAccount($resource, $provider)) {

                try {
                    // Check if an account already exists
                    $account = $this->oidcAccountRepository->findForResource($resource);
                    if (empty($account)) {

                        // No account exists: insert
                        $this->logger->debug("Linking a new account");
                        $accountId = $this->oidcAccountRepository->insert($this->loggedInUser, $resource);
                        $this->dispatchEvent(new AccountLinkedEvent($oidc->getResource(), $accountId));
                        $this->getUserStorage()->store($oidc, $accountId);
                        $this->storeActionResult('link_ok', $provider);

                    } elseif ($account['deleted']) {

                        // An account has been deleted: recycle the record
                        $this->logger->debug("Recycling a deleted account");
                        $accountId = $account['uid'];
                        if ($this->oidcAccountRepository->overwrite($accountId, $this->loggedInUser, $resource)) {
                            $this->dispatchEvent(new AccountLinkedEvent($oidc->getResource(), $accountId));
                            $this->getUserStorage()->store($oidc, $accountId);
                            $this->storeActionResult('link_ok', $provider);
                        } else {
                            $this->logger->error('Account linking has failed');
                            $this->storeActionResult('link_error', $provider);
                        }

                    } elseif ($account['user'] === $targetUser || $account['user'] === 0) {

                        // Another account of this user or an anonymous account: update it
                        // unless disabled and the user is not allowed to unlock itself
                        if (!$account['disable'] || $this->settings['enableOidcAccount']) {
                            $this->logger->debug("Updating an existing account");
                            $accountId = $account['uid'];
                            $this->oidcAccountRepository->update($accountId, $resource, $targetUser);
                            $this->dispatchEvent(new AccountUpdatedEvent($accountId, $resource));
                            $this->storeActionResult('update_ok', $provider);
                        } else {
                            $this->logger->info("cannot update a locked {provider} account {email}", [
                                'provider' => $provider,
                                'email' => $account['email']
                            ]);
                            $this->storeActionResult('update_locked', $provider, $account['email']);
                        }

                    } else {
                        // This account is currently linked to another user!
                        $this->logger->error('Account linking has failed: the account is already linked to another user');
                        $this->storeActionResult('account_exists', $provider);
                    }
                } catch (\Exception $e) {
                    $this->logger->error('Account linking has failed: ' . $e->getMessage());
                    $this->storeActionResult('link_error', $provider);
                }
            }
        }
        return $this->redirect('overview');
    }

    /**
     * Returns the currently authenticated user
     *
     * @return AbstractUserAuthentication
     */
    protected function getCurrentUser(): AbstractUserAuthentication
    {
        return $this->request->getAttribute("frontend.user");
    }

    /**
     * Returns an object that stores oidc data for the current user
     *
     * @return UserStorage
     */
    protected function getUserStorage(): UserStorage
    {
        return GeneralUtility::makeInstance(UserStorage::class, $this->user);
    }

    /**
     * Returns true if the currently logged-in user matches the requested one.
     *
     * In case of a failure it generates an error message that will be displayed
     * on the overview page.
     *
     * @param int $targetUser
     * @return bool
     */
    protected function validateLoggedInUser(int $targetUser): bool
    {
        if (!$this->loggedInUser) {
            $action = $this->request->getControllerActionName();
            $this->logger->warning("No logged in user detect: the $action action is ignored");
            $this->storeActionResult('no_user_error');
        } elseif ($targetUser !== $this->loggedInUser['uid']) {
            // user has changed
            $action = $this->request->getControllerActionName();
            $this->logger->warning("$action action requested by {target}, but processed by {current}", [
                'target' => $targetUser,
                'current' => $this->loggedInUser['uid']
            ]);
            $this->storeActionResult('user_changed');
        } else {
            return true;
        }
        return false;
    }

    /**
     * Returns true if the oauth authentication process has been successful.
     * Otherwise it generates an error message to be displayed on the overview page.
     *
     * @param OidcAspect|null $oidc
     * @return bool
     */
    protected function validateOidc(?OidcAspect $oidc): bool
    {
        if (!$oidc) {
            $action = $this->request->getControllerActionName();
            $this->logger->warning("ignoring $action action, because it is not preceded by an authentication process");
        } elseif (!$oidc->isAuthenticated()) {
            $this->storeActionResult('auth_failure', $oidc->get('error'));
        } else {
            return true;
        }
        return false;
    }

    /**
     * Returns the aspect with the result of the oauth authentication process
     *
     * @return OidcAspect|null
     */
    protected function getOidcAspect(): ?OidcAspect
    {
        $context = GeneralUtility::makeInstance(Context::class);
        return $context->hasAspect(Constants::ASPECT)
            ? $context->getAspect(Constants::ASPECT) : null;
    }

    /**
     * A helped method that stores a message for the overview page.
     *
     * This method does not generate the message, but saves the key
     * of a template message with parameters. The actual message is
     * then build by {@see getActionResult()}.
     *
     * @param string $msgKey      the key of the template message
     * @param mixed ...$context   parameters for the template
     * @return void
     */
    protected function storeActionResult(string $msgKey, mixed ...$context): void
    {
        $data = ['msg' => $msgKey];
        if ($context) $data['ctx'] = $context;
        $this->user->setAndSaveSessionData('tx_' . Constants::FEACCOUNT_PLUGIN, $data);
    }

    /**
     * Returns a message built from the data stored by {@see storeActionResult()}.
     *
     * @return array|null
     */
    protected function getActionResult(): ?array
    {
        // Extract the data from the session: once read
        // it should no longer be available
        $data = $this->user->getSessionData('tx_' . Constants::FEACCOUNT_PLUGIN);
        $this->user->setAndSaveSessionData('tx_' . Constants::FEACCOUNT_PLUGIN, null);

        // Evaluate the message
        if (!is_array($data) || empty($data['msg'])) return null;
        return $this->getMessage($data['msg'], $data['ctx'] ?? []);
    }

    /**
     * Loads a localized message template and fill it in with the provided parameters.
     *
     * @param string $key
     * @param array $context
     * @return array|null
     */
    protected function getMessage(string $key, array $context = []): ?array
    {
        $msgType = self::$messages[$key] ?? null;
        return $msgType ? [
            'type' => $msgType,
            'content' => LocalizationUtility::translate(
                "plugin.feaccount.$key",
                Constants::EXT_KEY,
                $context
            )] : null;
    }

    /**
     * A helper function that dispatches an event and creates a log entry.
     *
     * @param $event
     * @return void
     */
    protected function dispatchEvent($event): void
    {
        $this->logger->debug('dispatching ' . get_class($event));
        GeneralUtility::makeInstance(EventDispatcherInterface::class)->dispatch($event);
    }
}
