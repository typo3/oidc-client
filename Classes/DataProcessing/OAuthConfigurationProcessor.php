<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\DataProcessing;

use IMATHUZH\OidcClient\Service\Configuration;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Data processor that allows to access oidc related configuration:
 * - endpoint: the base path handled by the extension
 * - loginType: 'fe' for frontend and 'be' for backend
 *
 * This processor is required by fluid templates provided with this extension.
 */
class OAuthConfigurationProcessor implements DataProcessorInterface
{
    /**
     * @inheritDoc
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ): array {
        // Do not change if already processed
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }
        // Set the target variable
        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, 'oidc');
        // Get the configuration and prepare data
        $config = Configuration::getInstance();
        $processedData[$targetVariableName] = [
            'endpoint' => $config->oidcRouteBase() . Constants::ROUTE_LOGIN,
            'loginType' => $processorConfiguration['loginType'] ?? 'fe'
        ];

        return $processedData;
    }
}
