<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\DataProcessing;

use IMATHUZH\OidcClient\Utility\FlexFormUtility;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Data processor for the flex form data with configuration of
 * installed resource processors.
 */
class ResProcConfigProcessor implements DataProcessorInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @inheritDoc
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ): array {

        // Do not change if already processed
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }

        $sourceField = $cObj->stdWrapValue('fieldName', $processorConfiguration, 'processors');
        $processors = $processedData['data'][$sourceField];

        if ($processors) {
            $parsed = FlexFormUtility::flexData2array($processors);
        } else {
            $parsed = [];
        }

        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, 'processors');
        $processedData[$targetVariableName] = $parsed;
        return $processedData;
    }
}