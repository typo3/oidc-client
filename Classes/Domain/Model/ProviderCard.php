<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Model;

use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * The entity with properties of an oidc provider used to display
 * the provider card in the backend.
 */
class ProviderCard extends AbstractEntity
{
    protected string $name = '';

    protected string $title = '';

    protected ?FileReference $logo = null;

    protected string $description = '';

    protected int $backend = 0;

    protected int $frontend = 0;

    protected bool $mfa = false;

    /**
     * Returns unpacked data as an array. This method is used by BackendController
     * to provide authentication features as separate values instead of one number,
     * making it easier to create checkbox fields.
     *
     * @return array
     */
    public function unpack(): array
    {
        return [
            'uid' => $this->getUid(),
            'pid' => $this->getPid(),
            'name' => $this->name,
            'title' => $this->title,
            'logo' => $this->logo,
            'description' => $this->description,
            'backend' => self::unpackAuthConfig($this->backend),
            'frontend' => self::unpackAuthConfig($this->frontend),
            'mfa' => $this->mfa
        ];
    }

    /**
     * A helper function that converts an integer with flags
     * for authentication features into an associative array.
     *
     * @param int $config
     * @return int[]
     */
    protected static function unpackAuthConfig(int $config): array
    {
        return [
            'enabled' => $config & Constants::AUTH_ENABLED,
            'createUser' => $config & Constants::AUTH_CREATE_USER,
            'unlockUser' => $config & Constants::AUTH_UNLOCK_USER
        ];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $value): void
    {
        $this->name = $value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $value): void
    {
        $this->title = $value;
    }

    public function getLogo(): ?FileReference
    {
        return $this->logo;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $value): void
    {
        $this->description = $value;
    }

    public function getBackendConfig(): int
    {
        return $this->backend;
    }

    public function setBackendConfig(int $config): void
    {
        $this->backend = $config;
    }

    public function getBackendEnabled(): bool
    {
        return (bool)($this->backend && Constants::AUTH_ENABLED);
    }

    public function setBackendEnabled(bool $state): void
    {
        if ($state) {
            $this->backend |= Constants::AUTH_ENABLED;
        } else {
            $this->backend &= ~Constants::AUTH_ENABLED;
        }
    }

    public function getFrontendConfig(): int
    {
        return $this->frontend;
    }

    public function setFrontendConfig(int $config): void
    {
        $this->frontend = $config;
    }

    public function getFrontendEnabled(): bool
    {
        return (bool)($this->frontend && Constants::AUTH_ENABLED);
    }

    public function setFrontendEnabled(bool $state): void
    {
        if ($state) {
            $this->frontend |= Constants::AUTH_ENABLED;
        } else {
            $this->frontend &= ~Constants::AUTH_ENABLED;
        }
    }

    public function getMfa(): bool
    {
        return $this->mfa;
    }

    public function setMfa(bool $state): void
    {
        $this->mfa = $state;
    }
}
