<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Model;

use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * The entity with properties of an oidc provider necessary
 * to display its label, for instance on an authentication
 * button. This is a read-only entity.
 */
class ProviderLabel extends AbstractEntity
{
    protected string $title;
    protected string $name;
    protected ?FileReference $logo;
    protected int $frontend;
    protected int $backend;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogo(): ?FileReference
    {
        return $this->logo;
    }

    public function isEnabledForFrontend(): bool
    {
        return (bool)($this->frontend & Constants::AUTH_ENABLED);
    }

    public function isEnabledForBackend(): bool
    {
        return (bool)($this->backend & Constants::AUTH_ENABLED);
    }
}
