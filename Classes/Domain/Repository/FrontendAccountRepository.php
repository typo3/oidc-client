<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception as DBALExceptionBase;
use Doctrine\DBAL\Result;
use IMATHUZH\OidcClient\OAuth2\UserResource;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A repository for frontend oidc accounts
 */
class FrontendAccountRepository
{
    /** @var string the name of the table with oidc accounts */
    protected string $oidcAccountTable = Constants::TABLE_FEUSER;

    public function __construct(protected ConnectionPool $connectionPool)
    { }

    /**
     * Returns the oidc account by its uid
     */
    public function find(int $uid): array
    {
        return $this->findMatching(['uid' => $uid])
            ->fetchAssociative() ?: [];
    }

    /**
     * Returns all oidc accounts assigned to a Typo3 user
     *
     * @throws DBALException|DBALExceptionBase
     */
    public function findForUser(int $userId): array
    {
        return $this->findMatching(['user' => $userId])->fetchAllAssociative();
    }

    /**
     * Returns the oidc account matching the provided resource
     *
     * @param UserResource $resource
     * @return array
     * @throws DBALExceptionBase
     */
    public function findForResource(UserResource $resource): array
    {
        return $this->findMatching([
            'provider' => $resource->getProviderId(),
            'sub' => $resource->getId()
        ], true)->fetchAssociative() ?: [];
    }

    /**
     * A helper function that returns oidc account matching provided conditions.
     * The list of conditions is an associated array or required values with
     * field names as keys.
     *
     * @param array $conditions     an associative array with required values
     * @param bool $includeDeleted  if true, then deleted accounts are also returned
     * @return Result
     * @throws DBALException
     */
    protected function findMatching(array $conditions, bool $includeDeleted = false): Result
    {
        $connection = $this->connectionPool->getConnectionForTable($this->oidcAccountTable);
        $queryBuilder = $connection->createQueryBuilder();
        // Configure restrictions: we want to fetch all records, skipping probably only the deleted ones
        $restrictions = $queryBuilder->getRestrictions()->removeAll();
        if (!$includeDeleted) {
            $restrictions->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        }
        // Add conditions: an equality WHERE clause for each entry in $conditions
        $expr = $queryBuilder->expr();
        $where = array_map(
            fn ($field, $value) => $expr->eq($field, $queryBuilder->quote($value)),
            array_keys($conditions),
            array_values($conditions)
        );
        // Build and execute the query
        return $queryBuilder->select(
                'uid', 'sub', 'provider', 'user', 'email', 'last_auth', 'disable', 'deleted', 'crdate'
            )->from($this->oidcAccountTable)
            ->where(...$where)
            ->executeQuery();
    }

    /**
     * Deletes an oidc account. If `$userId` is non-zero, then the account is deleted
     * only when assigned to that user.
     *
     * @param int $accountUid
     * @param int $userId
     * @return int
     */
    public function delete(int $accountUid, int $userId=0): int
    {
        $filter = ['uid' => $accountUid];
        if ($userId) $filter['user'] = $userId;
        return $this->connectionPool->getConnectionForTable($this->oidcAccountTable)
            ->delete($this->oidcAccountTable, $filter);
    }

    /**
     * Unlinks an oidc account by filling `user` field with 0 (anonymous account).
     * If `$userId` is non-zero, then the account is unlinked only when assigned to that user.
     *
     * @param int $accountUid
     * @param int $userId
     * @return int
     */
    public function unlink(int $accountUid, int $userId=0): int
    {
        $filter = ['uid' => $accountUid];
        if ($userId) $filter['user'] = $userId;
        return $this->connectionPool->getConnectionForTable($this->oidcAccountTable)
            ->update($this->oidcAccountTable, ['user' => 0], $filter);
    }

    /**
     * Updates an oidc account:
     * - marks as accessed with the current timestamp
     * - sets `disable` and `deleted` to 0
     * - updates the email from the resource unless `email_verified` is `false`
     * - sets `user` to $userId if provided and non-zero
     *
     * @param int $accountId
     * @param UserResource $resource
     * @param int $userId
     * @return bool
     */
    public function update(int $accountId, UserResource $resource, int $userId=0): bool
    {
        // Prepare resources
        $data = [
            'disable' => 0,
            'deleted' => 0,
            'tstamp' => $GLOBALS['EXEC_TIME'],
            'last_auth' => $GLOBALS['EXEC_TIME']
        ];
        if ($resource->get('email_verified') !== false) {
            $data['email'] = $resource->get('email');
        }
        if ($userId) {
            $data['user'] = $userId;
        }

        return (bool)$this->connectionPool
            ->getConnectionForTable($this->oidcAccountTable)
            ->update($this->oidcAccountTable, $data, [
                'uid' => $accountId,
                'sub' => $resource->getId(),
                'provider' => $resource->getProviderId()
            ]);
    }

    /**
     * Return an associative array with values for a full record representing
     * an oidc account for the provided user and resource.
     *
     * @param array $user
     * @param UserResource $resource
     * @return array
     */
    private function createRecordFor(array $user, UserResource $resource): array
    {
        return [
            'pid' => $user['pid'],
            'user' => $user['uid'],
            'provider' => $resource->getProviderId(),
            'sub' => $resource->getId(),
            'last_auth' => $GLOBALS['EXEC_TIME'],
            'tstamp' => $GLOBALS['EXEC_TIME'],
            'crdate' => $GLOBALS['EXEC_TIME'],
            'email' => $resource->get('email_verified') === false ? '' : $resource->get('email'),
            // Add these properties in case an existing record is to be overwritten
            'deleted' => 0,
            'disable' => 0
        ];
    }

    /**
     * Inserts an oidc account for the provided user and resource and returns
     * the uid of the newly created record.
     * Throws when such an account already exists
     *
     * @param array $user
     * @param UserResource $resource
     * @return int
     */
    public function insert(array $user, UserResource $resource): int
    {
        $connection = $this->connectionPool->getConnectionForTable($this->oidcAccountTable);
        $connection->insert(
            $this->oidcAccountTable,
            $this->createRecordFor($user, $resource)
        );
        return (int)$connection->lastInsertId();
    }

    /**
     * Completely overwrites an existing record. It has the same effect as deleting
     * the existing record and creating a new one from the provided data, except that
     * the uid is preserved.
     *
     * @param int $accountId
     * @param array $user
     * @param UserResource $resource
     * @return bool                   true if a record has been updated and false if none exists
     */
    public function overwrite(int $accountId, array $user, UserResource $resource): bool
    {
        $connection = $this->connectionPool->getConnectionForTable($this->oidcAccountTable);
        return (bool)$connection->update(
            $this->oidcAccountTable,
            $this->createRecordFor($user, $resource),
            ['uid' => $accountId]
        );
    }

}