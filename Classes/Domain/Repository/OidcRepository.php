<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Repository;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Result;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * A general repository class used but the oauth service.
 *
 * This class will be refactored into several repositories along
 * the refactoring of the oauth service.
 */
class OidcRepository
{
    protected ConnectionPool $connectionPool;

    protected ?Connection $oidcAccountConnection = null;

    protected ?Connection $oidcPatternConnection = null;

    protected ?Connection $providerConnection = null;

    protected $oidc_table = '';

    protected $pattern_table = '';

    protected $user_table = '';

    public function __construct(
        ConnectionPool $connectionPool
    )
    {
        $this->connectionPool = $connectionPool;
    }

    public function setUserType(string $userType): void
    {
        $this->oidc_table = Constants::TABLE_PREFIX . $userType . 'user';
        $this->pattern_table = Constants::TABLE_PREFIX . $userType . 'group';
        $this->user_table = $userType . '_users';
    }

    protected function getAccountConnection(): Connection
    {
        if (!$this->oidcAccountConnection) {
            $this->oidcAccountConnection = $this->connectionPool->getConnectionForTable($this->oidc_table);
        }
        return $this->oidcAccountConnection;
    }

    protected function getPatternConnection(): Connection
    {
        if (!$this->oidcPatternConnection) {
            $this->oidcPatternConnection = $this->connectionPool->getConnectionForTable($this->pattern_table);
        }
        return $this->oidcPatternConnection;
    }

    protected function getProviderConnection(): Connection
    {
        if (!$this->providerConnection) {
            $this->providerConnection = $this->connectionPool->getConnectionForTable(Constants::TABLE_OIDC_CONFIG);
        }
        return $this->providerConnection;
    }

    /**********************************************/
    /*                                            */
    /*        OIDC provider configuration         */
    /*                                            */
    /**********************************************/

    /**
     * @throws Exception
     */
    public function findProviderConfigById(int $providerId): array
    {
        return $this->getProviderConnection()
            ->select(['*'], Constants::TABLE_OIDC_CONFIG, ['uid' => $providerId])
            ->fetchAssociative() ?: [];
    }

    /**
     * @throws Exception
     */
    public function findProviderConfigByName(string $providerName): array
    {
        $connection = $this->getProviderConnection();
        return $connection
            ->select(['*'], Constants::TABLE_OIDC_CONFIG, ['name' => $connection->quote($providerName)])
            ->fetchAssociative() ?: [];
    }

    /**
     * @throws Exception
     */
    public function findProviderConfig($provider): array
    {
        $providerId = MathUtility::convertToPositiveInteger($provider);
        if ($providerId) {
            return $this->findProviderConfigById($providerId);
        } elseif ($provider) {
            return $this->findProviderConfigByName($provider);
        } else {
            return $this->getProviderConnection()
                ->select(['*'], Constants::TABLE_OIDC_CONFIG, [], [], ['sorting' => 'ASC'], 1)
                ->fetchAssociative() ?: [];
        }
    }


    /**********************************************/
    /*                                            */
    /*        OIDC account management             */
    /*                                            */
    /**********************************************/

    /**
     * Returns the record of the oidc account and a user it is linked to.
     * If the record does not exist, then an empty array  is returned.
     */
    public function findAccount(int $providerId, string $sub): array
    {
        $queryBuilder = $this->getAccountConnection()->createQueryBuilder();
        $expr = $queryBuilder->expr();

        $row = $queryBuilder->select(
            'user.*',
            'oidc.disable AS oidcDisable',
            'oidc.deleted AS oidcDeleted',
            'oidc.user AS oidcUser',
            'oidc.uid AS oidcUid'
        )->from($this->oidc_table, 'oidc')
        ->leftJoin(
            'oidc', $this->user_table, 'user',
            $expr->eq('oidc.user', 'user.uid')
        )->where(
            $expr->eq('oidc.sub', $queryBuilder->quote($sub)),
            $expr->eq('oidc.provider', $providerId)
        )->execute()
        ->fetchAssociative();

        if ($row) {
            $oidc_keys = array_flip([ 'oidcDisable', 'oidcDeleted', 'oidcUser', 'oidcUid' ]);
            $oidc = [
                'uid' => $row['oidcUid'],
                'sub' => $sub,
                'provider' => $providerId,
                'disable' => $row['oidcDisable'],
                'deleted' => $row['oidcDeleted'],
                'user' => array_diff_key($row, $oidc_keys)
            ];
        } else {
            $oidc = [];
        }
        return $oidc;
    }

    /**
     * Inserts a new OIDC account into the repository
     */
    public function insertAccount(array $account): int
    {
        return $this->getAccountConnection()->insert(
            $this->oidc_table, [
                'pid' => $account['pid'] ?? 0,
                'sub' => $account['sub'],
                'provider' => $account['provider'],
                'user' => $account['user'],
                'tstamp' => $GLOBALS['EXEC_TIME'],
                'crdate' => $GLOBALS['EXEC_TIME']
            ], [
                Connection::PARAM_INT,
                Connection::PARAM_STR,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT
            ]
        );
    }

    /**
     * Updates the associated user and enables the OIDC account when disabled.
     * The record is recycled if it is deleted.
     */
    public function updateAndEnableAccount(int $accountUid, int $userId): int
    {
        return $this->getAccountConnection()->update(
            $this->oidc_table, [
                'user' => $userId,
                'deleted' => 0,
                'disable' => 0,
                'tstamp' => $GLOBALS['EXEC_TIME']
            ], [
                'uid' => $accountUid
            ], [
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT
            ]
        );
    }

    /**
     * Moves oidc accounts of one user to another one and updates the groups
     * of the latter.
     */
    public function moveAccountsBetweenUsers(int $sourceUid, int $targetUid): int
    {
        return $this->getAccountConnection()->update(
            $this->oidc_table, [
                'user' => $targetUid,
                'tstamp' => $GLOBALS['EXEC_TIME']
            ], [
                'user' => $sourceUid,
                'delete' => 0
            ], [
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT,
                Connection::PARAM_INT
            ]
        );
    }


    /**********************************************/
    /*                                            */
    /*              Group patterns                */
    /*                                            */
    /**********************************************/

    /**
     * Iterates over group patterns configured for a provider.
     */
    public function groupPatterns(int $providerId, bool $persistentOnly = false): Result
    {
        // Deleted and disabled patterns should be omitted.
        // Hence, the simple select() method is sufficient
        $restrictions = ['provider' => $providerId];
        if ($persistentOnly) $restrictions['persistent'] = 1;
        return $this->getPatternConnection()->select(
            ['*'], $this->pattern_table, $restrictions
        );
    }

    /**
     * Iterates over groups patterns configured for providers, from which
     * the user has an oidc account.
     */
    public function groupPatternsForUser(int $uid, bool $persistentOnly = false): Result
    {
        // Fetch patterns associated with those providers for which the user
        // has an active oidc account
        $query = $this->getPatternConnection()->createQueryBuilder();
        $expr = $query->expr();
        $query->select('pattern.*')
            ->from($this->pattern_table, 'pattern')
            ->join('pattern', $this->oidc_table, 'oidc', $expr->eq('oidc.provider', 'pattern.provider'))
            ->where($expr->eq('oidc.user', $uid));
        if ($persistentOnly) $query->andWhere($expr->eq('pattern.persistent', 1));
        
        return $query->execute();
    }

    /**
     * Returns only those groups that will stay after authenticating
     * with a given provider. You can pass 0 as the argument to remove
     * all groups that require an authentication by any OIDC provider.
     */
    public function filterPersistentGroups(array $groups, int $provider): array
    {
        // Using a query builder, because of a complex WHERE clause
        $queryBuilder = $this->getPatternConnection()->createQueryBuilder();
        $expr = $queryBuilder->expr();
        return $queryBuilder->select('group')
            ->from($this->pattern_table)
            ->where(
                $expr->in('group', $groups),
                $provider ? $expr->or(
                    $expr->neq('provider', $provider),
                    $expr->eq('persistent', 1)
                ) : $expr->eq('persistent', 1)
            )->execute()
            ->fetchFirstColumn();
    }

}
