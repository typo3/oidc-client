<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Repository;

use Doctrine\DBAL\DBALException;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * A repository with entities holding data of oidc providers required
 * to display the provider cards in the main view in the backend.
 */
class ProviderCardRepository extends Repository implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var string  The name of the table with oidc providers.
     *
     * This must be specified, because the table name does not
     * follow the Extbase convention.
     */
    protected string $table = Constants::TABLE_OIDC_CONFIG;

    /** @var array The field used for sorting results of queries */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject(): void
    {
        $querySettings = GeneralUtility::makeInstance(Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(false);
        $querySettings->setIgnoreEnableFields(true);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * Updates the authentication features of a provider
     *
     * @param int $uid
     * @param int $frontend
     * @param int $backend
     * @param bool $mfa
     * @return void
     */
    public function updateConfiguration(int $uid, int $frontend, int $backend, bool $mfa): void
    {
        // Extbase repositories do not allow to update an object
        // directly when uid is known. We have to do this directly.
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($this->table)
            ->update($this->table, [
                'frontend' => $frontend,
                'backend' => $backend,
                'mfa' => (int)$mfa
            ], ['uid' => $uid]);
    }


    /**
     * Reorders the records with configuration of providers by moving
     * the record $uid to the place of $target, while keeping the ordering
     * of other records unchagned. Depending on the relative ordering,
     * this means inserting the record $uid before or after $target.
     *
     * @param int $uid
     * @param int $target
     * @return void
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function reorder(int $uid, int $target)
    {
        if ($uid === $target) return;

        // This is easier to do with direct queries
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($this->table);

        // Get the sorting range (values of the `sorting` field)
        $range = $this->getSortingRange($connection, $uid, $target);
        if (empty($range)) {
            $this->logger->warning("no element with uid $uid or $target has been found");
            return;
        }

        list($currentSorting, $targetSorting) = $range;
        if ($currentSorting === $targetSorting) {
            $this->logger->debug("the element $uid is already in the desired position");
            return;
        }
        
        // Reordering requires more than one SQL statement.
        // We use transactions to enforce consistency of the data.
        $connection->beginTransaction();
        try {
            // We need to shift all records in the range by the same value.
            // To find the shift distance we have to find the direction of
            // the movement and the `sorting` value of the record that will
            // be placed in the position of $uid.
            $dir = $currentSorting < $targetSorting ? 'ASC' : 'DESC';
            $nextSorting = $this->getSortingOfNextRecord($connection, $uid, $dir);
            if ($nextSorting < 0) {
                $this->logger->debug("the element $uid is already in the desired position");
                return;
            }
            $sortingShift = $currentSorting - $nextSorting;
            // We can now shift the records
            $this->shiftRecords($connection, $uid, $currentSorting, $targetSorting, $sortingShift);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * Returns a pair (current.sorting, target.sorting) or null
     * when either element is not found.
     */
    protected function getSortingRange(Connection $connection, int $uid, int $target): ?array
    {
        $query = $connection->createQueryBuilder();
        $query->getRestrictions()->removeAll();

        return $query->select('C.sorting', 'T.sorting')
            ->from($this->table, 'T')
            ->join('T', $this->table, 'C', "C.uid = $uid")
            ->where("T.uid = $target")
            ->executeQuery()
            ->fetchNumeric() ?: null;
    }

    /**
     * Returns the sorting value for the next record relative to the desired direction
     */
    protected function getSortingOfNextRecord(Connection $connection, int $uid, string $dir = 'ASC'): int
    {
        $cmp = $dir === 'ASC' ? '<' : '>';
        $query = $connection->createQueryBuilder();
        $query->getRestrictions()->removeAll();

        $result = $query->select('A.sorting')
            ->from($this->table, "A")
            ->join('A', $this->table, 'C', "C.uid = $uid")
            ->where("C.sorting $cmp A.sorting")
            ->orderBy('sorting', $dir)
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchOne();

        return $result === false ? -1 : (int)$result;
    }

    /**
     * Adjusts the `sorting` field of records in the provided range by a given value
     * except $uid, in which case the field is updated to $targetSorting.
     *
     * @param Connection $connection
     * @param int $uid                  the record to be moved over the shifted region
     * @param int $currentSorting       `sorting` field of the record $uid
     * @param int $targetSorting        `sorting` field of the other end of the range
     * @param int $sortingShift         shift distance
     * @return void
     * @throws DBALException
     */
    protected function shiftRecords(Connection $connection, int $uid, int $currentSorting, int $targetSorting, int $sortingShift): void
    {
        // Update `sorting` fields for the records in the range
        $cmp = $currentSorting < $targetSorting ? '<' : '>';
        $query = $connection->createQueryBuilder();
        $query->getRestrictions()->removeAll();
        $moved = $query->update($this->table)
            ->set('sorting', "sorting + $sortingShift", false)
            ->where(
                "$currentSorting $cmp sorting",
                "sorting $cmp= $targetSorting"
            )->executeStatement();
        // At least one record must've been moved - should we check this?

        // Update the $uid record
        $query = $connection->createQueryBuilder();
        $query->getRestrictions()->removeAll();
        $query->update($this->table)
            ->set('sorting', $targetSorting)
            ->where("uid = $uid")
            ->executeStatement();
    }
}
