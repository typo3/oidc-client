<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Domain\Repository;

use IMATHUZH\OidcClient\Domain\Model\ProviderLabel;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository of entities storing properties of oidc providers
 * required to display their labels, for instance on authentication
 * buttons.
 */
class ProviderLabelRepository extends Repository
{
    /**
     * @var string  The name of the table with oidc providers.
     *
     * This must be specified, because the table name does not
     * follow the Extbase convention.
     */
    protected string $table = Constants::TABLE_OIDC_CONFIG;

    /** @var array The field used for sorting results of queries */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Fetches providers that are enabled for the backend authentication.
     *
     * @return array
     */
    public function findForBackend(): array
    {
        // Extbase does not support bitwise operations...
        return array_filter(
            $this->createQuery()->execute()->toArray(),
            fn (ProviderLabel $provider) => $provider->isEnabledForBackend()
        );
    }

    /**
     * Fetches providers that are enabled for the frontend authentication.
     * If the parameter $uids is specified, then the result is restricted
     * to records with uid from this set.
     *
     * @param array|null $uids
     * @return array
     */
    public function findForFrontend(?array $uids = null): array
    {
        // Extbase does not support bitwise operations...
        return array_filter(
            is_array($uids) ? $this->findByUidFrom($uids) : $this->findAll()->toArray(),
            fn (ProviderLabel $provider) => $provider->isEnabledForFrontend()
        );
    }

    /**
     * Fetches providers with matching uid.
     *
     * @param array $uids
     * @return array
     */
    public function findByUidFrom(array $uids): array
    {
        $query = $this->createQuery();
        $query->matching($query->in('uid', $uids));
        return $query->execute()->toArray();
    }
}
