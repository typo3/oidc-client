<?php

namespace IMATHUZH\OidcClient\Domain\Repository;

use Doctrine\DBAL\Result;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TTContentRepository
{
    public function __construct(
        protected ConnectionPool $connectionPool
    ) { }

    public function findOnPage(string $CType, int $pageId, array $fields = ['*']): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tt_content');
        $expr = $queryBuilder->expr();
        $whereCType = $expr->in('CType', [$queryBuilder->quote($CType), "'shortcut'"]);

        $fields = array_unique(array_merge($fields, ['CType', 'records']));
        $stm = $queryBuilder
            ->select(...$fields)
            ->from('tt_content')
            ->where($expr->eq('pid', $pageId), $whereCType);

        return $this->findRecursively($CType, $fields, $whereCType, $stm->executeQuery());
    }

    protected function findRecursively(string $CType, array $fields, string $whereCType, Result $elements): array
    {
        while ($element = $elements->fetchAssociative()) {
            if ($element['CType'] === $CType) {
                return $element;
            }

            $uids = GeneralUtility::intExplode(',', $element['records'], true);
            if ($uids) {
                $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tt_content');
                $expr = $queryBuilder->expr();

                $stm = $queryBuilder
                    ->select(...$fields)
                    ->from('tt_content')
                    ->where($expr->in('uid', $uids), $whereCType);

                $element = $this->findRecursively($CType, $fields, $whereCType,  $stm->executeQuery());
                if ($element) {
                    return $element;
                }
            }
        }
        return [];
    }
}