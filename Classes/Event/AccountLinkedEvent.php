<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Event;

use IMATHUZH\OidcClient\OAuth2\UserResource;

/**
 * An event dispatched after a new oauth account has been linked
 * to a local typo3 account.
 *
 * The linking can be prevented by capturing {@link BeforeLinkAccountEvent}.
 */
class AccountLinkedEvent
{
    public function __construct(
        protected UserResource $resource,
        protected int $accountId
    ) { }

    public function getResource(): UserResource
    {
        return $this->resource;
    }

    public function getAccountUid(): int
    {
        return $this->accountId;
    }
}
