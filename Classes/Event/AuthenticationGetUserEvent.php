<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Event;

/**
 * A PSR-14 event dispatched after a user is created from a resource owner.
 */
final class AuthenticationGetUserEvent
{
    protected array $user;

    /**
     * The data of the user
     * @param array $user
     */
    public function __construct(array $user)
    {
        $this->user = $user;
    }

    /**
     * The current user data, initialized with the data
     * of a successfully authenticated user
     * @return array
     */
    public function getUser(): array
    {
        return $this->user;
    }

    /**
     * Overrides the user data
     * @param array $user
     */
    public function setUser(array $user): void
    {
        $this->user = $user;
    }
}
