<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Event;

use IMATHUZH\OidcClient\OAuth2\UserResource;
use Psr\EventDispatcher\StoppableEventInterface;

/**
 * An event dispatched when an oauth account is about to be linked
 * to a local Typo3 account.
 *
 * 3rd parties can you this event to prevent the linking by calling
 * the {@link cancelLinking()} method:
 *
 * ```
 * if (!$currentUser->isAdmin()) {
 *   $event->cancelLinking("Only admin users can use this kind of accounts")
 * }
 * ```
 */
class BeforeLinkAccountEvent implements StoppableEventInterface
{
    protected bool $cancelled = false;

    protected string $reason = '';

    public function __construct(protected UserResource $resource)
    { }

    /**
     * @inheritDoc
     */
    public function isPropagationStopped(): bool
    {
        return $this->cancelled;
    }

    public function cancelLinking(string $reason): void
    {
        $this->cancelled = true;
        $this->reason = $reason;
    }

    public function isLinkingCancelled(): bool
    {
        return $this->cancelled;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getResource(): UserResource
    {
        return $this->resource;
    }
}
