<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Event;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\OAuth2\UserResource;
use League\OAuth2\Client\Token\AccessToken;
use phpDocumentor\Reflection\Types\Void_;

/**
 * A PSR-14 event dispatched after a resource owner is returned by
 * an oidc provider and before a typo3 user is created.
 */
final class ModifyResourceOwnerEvent
{
    public function __construct(
        protected UserResource $resourceOwner,
        protected AccessToken $accessToken,
        protected OidcAspect $aspect,
        protected string $loginType,
        protected array $processorsConfig
    ) { }

    /**
     * The resource owner returned by the oidc provider
     * @return UserResource
     */
    public function getResourceOwner(): UserResource
    {
        return $this->resourceOwner;
    }

    /**
     * The oidc aspect with properties of the current provider
     * and authentication features
     *
     * @return OidcAspect
     */
    public function getAspect(): OidcAspect
    {
        return $this->aspect;
    }

    /**
     * The current access token for the resource owner
     * @return AccessToken
     */
    public function getAccessToken(): AccessToken
    {
        return $this->accessToken;
    }

    public function getLoginType(): string
    {
        return $this->loginType;
    }

    public function getProcessorConfig(string $procId): array
    {
        return $this->processorsConfig[$procId] ?? ['enabled' => 0];
    }
}
