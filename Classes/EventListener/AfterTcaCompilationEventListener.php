<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\EventListener;

use IMATHUZH\OidcClient\Utility\Constants;
use IMATHUZH\OidcClient\Utility\FlexFormUtility;
use IMATHUZH\OidcClient\Utility\PluginManager;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Configuration\Event\AfterTcaCompilationEvent;
use TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools;

final class AfterTcaCompilationEventListener implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __invoke(AfterTcaCompilationEvent $event): void
    {
        $tca = $event->getTca();
        $this->updateFeloginPluginConfig($tca['tt_content']['columns']['pi_flexform']['config']['ds']);
        $this->updateProcessorsConfig($tca[Constants::TABLE_OIDC_CONFIG]);
        $event->setTca($tca);
    }

    private function updateFeloginPluginConfig(array &$piFlexFormDs): void
    {
        $feloginForm = $piFlexFormDs['*,felogin_login'] ?? null;
        if (empty($feloginForm)) return;

        $extkey = Constants::EXT_KEY;

        $feloginForm = FlexFormUtility::loadFlexForm($feloginForm);
        $oidcForm = FlexFormUtility::loadFlexForm("FILE:EXT:$extkey/Configuration/FlexForms/OidcButtons.xml");
        if (!$feloginForm || !$oidcForm) return;

        if (isset($oidcForm['sheets'])) {
            $feloginForm['sheets'] = array_merge($feloginForm['sheets'], $oidcForm['sheets']);
        } elseif(isset($oidcForm['ROOT'])) {
            $feloginForm['sheets']['s_OIDC'] = $oidcForm;
        } else {
            // Invalid format
            return;
        }
        $flexTools = new FlexFormTools();
        $piFlexFormDs['*,felogin_login'] = $flexTools->flexArray2Xml($feloginForm);
    }

    private function updateProcessorsConfig(array &$tca): void
    {
        $flexForm = PluginManager::generateProcessingConfigurationForm();
        if (empty($flexForm)) return;
        
        $label = 'LLL:EXT:' . Constants::EXT_KEY . '/Resources/Private/Language/locallang_db.xlf:config.processors';
        $description = "$label.description";
        if (count($flexForm['sheets']) === 1) {
            // Get the sheet without knowing its name
            $sheet = reset($flexForm['sheets'])['ROOT'];
            $label = $sheet['sheetTitle'] ?? $label;
            $description = $sheet['sheetDescription'] ?? $description;
        }

        $config = (new FlexFormTools())->flexArray2Xml($flexForm);
        $tca['columns']['processors'] = [
            'label' => $label,
            'description' => $description,
            'config' => [
                'type' => 'flex',
                'ds' => [ 'default' => $config ]
            ]
        ];
        $tca['types'][0]['showitem'] = str_replace(
            ', mapping,',
            ', mapping, processors,',
            $tca['types'][0]['showitem']
        );
    }
}
