<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\EventListener;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Domain\Repository\ProviderLabelRepository;
use IMATHUZH\OidcClient\Domain\Repository\TTContentRepository;
use IMATHUZH\OidcClient\Service\Configuration;
use IMATHUZH\OidcClient\Service\OAuthService;
use IMATHUZH\OidcClient\Utility\Constants;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\FrontendLogin\Event\ModifyLoginFormViewEvent;

final class FrontendLoginEventListener implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected ?OidcAspect $aspect = null;

    public function __construct(
        protected ProviderLabelRepository $buttonRepository,
        protected FlexFormService $flexFormService,
        protected Context $context,
        protected OAuthService $oauthService,
        protected TTContentRepository $repository
    ) {
        if ($context->hasAspect(Constants::ASPECT)) {
            $this->aspect = $context->getAspect(Constants::ASPECT);
        }
    }

    public function __invoke(ModifyLoginFormViewEvent $event): void
    {
        // Get the list of providers to be displayed
        $settings = $this->getSettings($this->getCurrentPageId());
        $providers = GeneralUtility::intExplode(',', $settings['provider'] ?? '', true);
        if (empty($providers)) return;

        // The login form needs not only providers, but also the OIDC endpoint
        $buttons = $this->buttonRepository->findForFrontend($providers);
        $config = Configuration::getInstance();
        $view = $event->getView();
        $view->assignMultiple([
            'oidcProviders' => $buttons,
            'oidcEndpoint' => $config->oidcRouteBase() . Constants::ROUTE_LOGIN,
            'oidcSettings' => $this->oauthService->encodeSettings([
                'storage' => $settings['pages'],
                'recursive' => intval($settings['recursive']),
                'unlock' => $settings['enableOidcAccount']
            ]),
        ]);
        if (($oidcError = $this->getAuthError())) {
            $view->assignMultiple([
                'oidcError' => $oidcError,
                'messageKey' => 'error'
            ]);
        }        
    }

    /**
     * Returns the id of the current page.
     *
     * The page id is used to fetch the configuration of the login form on this page.
     * It would be best to do it without referring to $GLOBALS.
     * @return int
     */
    private function getCurrentPageId(): int
    {
        return $GLOBALS['TSFE']->id;
    }

    private function isFrontedUserLoggedIn(): bool
    {
        try {
            return $this->context->getPropertyFromAspect('frontend.user', 'isLoggedIn');
        } catch (AspectNotFoundException $e) {
            // No aspect -> no logged-in user
            return false;
        }
    }

    /**
     * Searches for a login form on the page with the provided id and returns
     * the configuration of the plugin. This method returns an empty array if
     * no login form is found.
     *
     * @param int $pageId
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function getSettings(int $pageId): array
    {
        $config = $this->repository
            ->findOnPage('felogin_login', $pageId, ['pi_flexform']);

        return $config ? $this->flexFormService->convertFlexFormContentToArray($config['pi_flexform'])['settings']
                       : [];
    }

    /**
     * Returns the authentication error if any
     */
    private function getAuthError(): string
    {
        if (!$this->aspect || (
            $this->aspect->isAuthenticated()
            && $this->isFrontedUserLoggedIn()
        )) {
            return '';
        }

        /**
         * @todo Avoid duplication by moving this code into OidcAspect
         */
        $error = $this->aspect->get('error_description', '')
              ?: $this->aspect->get('error', '');
        // Decode if a JSON object
        if ($error && $error[0] === '{') {
            $data = \json_decode($error);
            if ($data) {
                $error = $data->error_description ?: $data->error ?: '';
            } else {
                $error = '';
            }
        }

        $provider = $this->aspect->getProviderTitle();
        return "The authentication " . ($provider ? "against $provider " : '')
              . "has failed" . ($error ? ": $error." : '.');
    }
}
