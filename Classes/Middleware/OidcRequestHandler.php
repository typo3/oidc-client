<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Middleware;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Exception\OAuthException;
use IMATHUZH\OidcClient\Service\Configuration;
use IMATHUZH\OidcClient\Service\OAuthService;
use IMATHUZH\OidcClient\Service\UserStorage;
use IMATHUZH\OidcClient\Utility\Constants;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Controller\ErrorPageController;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Http\HtmlResponse;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The processor for oidc client requests.
 *
 * It intercepts requests to the oidc endpoint (/oidc/ by default) or POST
 * requests with parameter oidcLoginType (to intercept backend login requests):
 * - login requests (/oidc/login or with oidcLoginType) initiate an authentication
 *   against an oidc provider specified by the oidcProvider parameter by storing
 *   oidc related data in a session and redirecting to the provider's page
 * - verify requests (/oidc/verify) take and verify callback requests from providers
 *   and, when successful, initiate authentication process
 */
final class OidcRequestHandler implements MiddlewareInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Configuration extension settings
     */
    private Configuration $config;

    protected ServerRequestInterface $request;

    protected string $requestTarget;

    protected array $params;


    public function __construct()
    {
        $this->config = Configuration::getInstance();
    }

    /**
     * Processes an oidc request before a user is authenticated.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Initialize parameters
        $this->request = $request;
        $this->requestTarget = explode('?', $request->getRequestTarget(), 2)[0];

        // A callback from the identity provider: process the code and store
        // the result in the oidc session, then redirect to the origin page.
        if ($this->isOidcVerify()) {
            $this->logger->debug("OIDC Stage: verify");
            return $this->processResponse($request->getQueryParams());
        }

        $params = array_merge(
            $request->getQueryParams() ?? [],
            $request->getParsedBody() ?? []
        );
        try {
            if (isset($params[Constants::OIDC_PARAM_PROVIDER])) {
                $this->logger->debug("OIDC Stage: initial");
                return $this->processAuth($params);
            } elseif (($state = $params[Constants::OIDC_PARAM_STATE] ?? null)) {
                // A redirection after the code has been processed
                $this->logger->debug("OIDC Stage: redirected");
                return $this->processRedirected($state, $request, $handler);
            } else {
                $this->logger->debug("Non OIDC request " . $request->getRequestTarget());
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            OidcAspect::createError('server error');
        }
        return $handler->handle($request);
    }

    protected function isOidcVerify(): bool
    {
        $callbackPath = $this->config->oidcRouteBase() . Constants::ROUTE_CALLBACK;
        return $this->requestTarget === $callbackPath;
    }

    protected function isFrontendRequest(): bool
    {
        return ApplicationType::fromRequest($this->request)->isFrontend();
    }


    protected function processAuth(array $params): ResponseInterface
    {
        $provider = $params[Constants::OIDC_PARAM_PROVIDER] ?? '';
        $requestId = self::getUniqueId($provider);
        $oauthService = GeneralUtility::makeInstance(OAuthService::class);
        $host = $this->request->getAttribute('normalizedParams')->getRequestHost();
        $alreadyStarted = $oauthService->initialize($provider, $requestId, $host);

        if ($alreadyStarted) {
            $this->logger->debug("the authentication process $requestId has already started - reusing the already computed authorization link");
            return new RedirectResponse($oauthService->getAuthorizationUrl(), 303);
        } else {
            $oidcParams = $oauthService->decodeSettings($params[Constants::REQUEST_PARAM] ?? null);
            $oauthParams = self::asArray($params[Constants::OAUTH_PARAM] ?? null);
            $requestType = $this->isFrontendRequest() ? 'frontend' : 'backend';

            $this->logger->debug("initializing the authentication process $requestId", [
                'provider' => $provider,
                'parameters' => $oidcParams,
                'request_type' => $requestType,
                'request_parameters' => $params
            ]);
            $authUrl = $oauthService->buildAuthorizationUrl($oauthParams);
            $oidcParams['provider'] = $provider;
            $oidcParams['provider_id'] = $oauthService->getProviderId();
            $oidcParams['request_type'] = $requestType;
//            $oidcParams['target_url'] = $this->getOriginUrl($params, $oauthService->getState());
            $oidcParams['target_url'] = $this->buildFinalUrl($oauthService->getState(), $params);
            $oauthService->setParameters($oidcParams);
            $tokenCookie = $oauthService->persist();
            return new RedirectResponse(
                $authUrl, 303, ['Set-Cookie' => $tokenCookie]
            );
        }
    }

    // No longer used. Reasons:
    // - redirecting to /typo3/login does not work in backend
    // - POST params must not be delivered via a query string
    protected function getOriginUrl(array $params, string $state): string
    {
        // oidc_redirect_url is currently used for backend login,
        // because the authentication does not work if the page is
        // redirected to the login form
        $url = ($params['oidc_redirect_url'] ?? null) ?: $this->requestTarget;
        $query = array_diff_key($params,
            array_flip([
                Constants::OIDC_PARAM_PROVIDER,
                Constants::REQUEST_PARAM,
                Constants::OAUTH_PARAM
            ])
        );
        $query[Constants::OIDC_PARAM_STATE] = $state;
        return $url . '?' . http_build_query($query);
    }

    /**
     * The final steps of the authentication flow: requesting an access token, then fetching
     * and processing user resources.
     * 
     * This method assumes that a parameter 'state' is present and a valid oidc session.
     * If this is not the case, then it stops, letting Typo3 continue to process the request.
     * Rationale:
     * - missing 'state' indicates that the page is not opened within an authentication flow
     * - missing session may be the result of reloading the page after the authentication
     *   has been processed completely
     */
    protected function processResponse(array $params): ResponseInterface
    {
        // We cannot proceed further if 'state' is missing, we even do not know the url
        // to process authentication errors. Display a generic error page in this case.
        if (empty($params['state'])) {
            return $this->errorResponse(
                400, 'Invalid request',
                'The page expected to process an authentication data from an external provider, but the required parameter "state" is missing. Did you open the link directly?'
            );
        }

        // We expose the 'state' parameter, because it is used to control
        // the flow of the authentication process.
        $state = $params['state'];
        unset($params['state']);

        $this->logger->debug("Processing OIDC callback with state $state");

        $oauthService = GeneralUtility::makeInstance(OAuthService::class);
        $oauthService->restore($state);
        $this->logger->info("Restored the oauth service $state.");

        // Prepare an oidc aspect for the page context
        $aspect = $oauthService->getAspect();
        $response = new RedirectResponse($oauthService->get('target_url'), 303);

        // Check if the provider is enabled
        if (!$oauthService->isEnabled()) {
            $this->logger->debug("authentication has failed", [
                'error' => 'disabled provider',
                'description' => "the identity provider " . $aspect->getProviderName() . " is disabled"
             ]);
             $aspect->setError(
                 'disabled provider',
                 'the identity provider ' . $aspect->getProviderTitle() . ' cannot be used in '
                    . $oauthService->get('request_type')
             );
             $oauthService->persist();
             return $response;
        }

        if (empty($params['code'])) {
            // No code provided - the user was not authenticated due to an error
            $this->logger->debug("authentication has failed", [
               'error' => $params['error'] ?? 'unknown error',
               'description' => $params['error_description'] ?? ''
            ]);
            $aspect->setError(
                $params['error'] ?? 'unknown error',
                $params['error_description'] ?? ''
            );
            $oauthService->persist();
            return $response;
        }

        // Process the code and delete the cookie
        try {
            $this->logger->debug("Processing authorization code");
            $resource = $oauthService->processAuthorizationCode($params['code']);
            $this->logger->debug("Received a resource: ", $resource->getClaims());
            $aspect->setResource($resource);
        } catch (OAuthException $oauthError) {
            $this->logger->warning("code processing failed: " . $oauthError->getMessage());
            $aspect->setError('oauth error', "the identity provider rejected the request");
        } catch (IdentityProviderException $e) {
            $msg = $e->getMessage();
            $body = $e->getResponseBody();
            if (is_array($body) || is_object($body)) {
                $body = json_encode($body, JSON_PRETTY_PRINT);
            }
            $this->logger->error("provider has rejected the request: $msg. Details: $body");
            $aspect->setError($msg, "the identity provider rejected the request");
        } catch (\Exception $e) {
            $this->logger->error("Unexpected error when proceeding code: " . $e->getMessage());
            $aspect->setError("server error", "the server failed to process the authentication data");
        } finally {
            $oauthService->persist();
            return $response;
        }
    }

    protected function processRedirected(string $state, ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Restore the authentication process. Return silently if no session is found,
        // because this could be triggered by reloading the page with query parameters
        // unchanged. In case no user is authenticated an error will be generated by
        // the authentication service.
        try {
            $oauthService = GeneralUtility::makeInstance(OAuthService::class);
            $oauthService->restore($state);
            $this->logger->debug("Restored the process " . $oauthService->getRequestId());
        } catch (OAuthException $oauthError) {
            return $handler->handle($request);
        }

        $aspect = $oauthService->getAspect();
        $this->logger->debug(implode("\n\t", [
            "Loaded aspect:",
            "authenticated: " . ($aspect->isAuthenticated() ? 'YES' : 'NO'),
            "resource id:   " . $aspect->getResourceId(),
            "error:         " . $aspect->get('error', '(none)')
        ]));
        $response = $handler->handle($request);
        $this->logger->debug("OAuth error: " . $aspect->get('error', '(none)'));
        if ($aspect->isAuthenticated()) {
            // Either we are logged in or the user has been authenticated with this provider
            $this->updateUserSession($aspect, $oauthService->hasMfa());
        }

        if (($location = self::getLocationHeader($response))) {
            $this->logger->debug("Redirection to $location");
            $oauthService->persist();
            return $response->withHeader('location', self::withAddedQuery(
                $location, [Constants::OIDC_PARAM_STATE => $state]
            ));
        } else {
            $this->logger->debug("Cleaning up " . $oauthService->getRequestId());
            return $response->withAddedHeader('Set-Cookie', $oauthService->tearDown());
        }
    }

    /**
     * @todo implement
     */
    protected function processProxy()
    {
    
    }

    /**
     * @todo implement
     */
    protected function processForward()
    {
        
    }

    /**
     * {@see ErrorController::handleDefaultError()}
     */
    protected function errorResponse(int $statusCode, string $title, string $message): ResponseInterface
    {
        if (str_contains($this->request->getHeaderLine('Accept'), 'application/json')) {
            return new JsonResponse(['reason' => $message], $statusCode);
        }
        $content = GeneralUtility::makeInstance(ErrorPageController::class)
            ->errorAction($title, $message, 0, $statusCode);
        return new HtmlResponse($content, $statusCode);
    }

    /**
     * Searches for the URL of the page to be displayed after a successful
     * authentication and saves it in the oidc session. The order of priority:
     * - parameter 'redirect_url'
     * - parameter 'referer'
     * - extension setting redirectUrl (frontend or backend)
     * - the root page
     *
     * @param string $loginType
     * @param string $state
     * @param bool $isTypo3login
     * @return string
     */
    protected function buildFinalUrl(string $state, array $params): string
    {
        $url = ($params['redirect_url'] ?? null)
            ?: ($params['referer'] ?? null);

        if ($this->isFrontendRequest()) {
            $url = $url ?: $this->config->frontend()->redirectUrl()
                        ?: $this->requestTarget;
            $loginParam = 'logintype';
        } else {
            $url = $url ?: $this->config->backend()->redirectUrl()
                        ?: preg_replace('/\/typo3\/.*$/', '/typo3/main', $this->requestTarget);
            $loginParam = 'login_status';
        }

        // Build the query string:
        // - add logintype / login_status if in $params
        // - add OIDC_PARAM_STATE
        // - remove cHash if present
        $additionalParams = array_intersect_key($params, [$loginParam => 1]);
        $additionalParams[Constants::OIDC_PARAM_STATE] = $state;

        return self::withAddedQuery($url, $additionalParams);
    }

    protected function buildErrorUrl(array $params): string
    {
        return empty($params['error_url'])
            ? $this->request->getRequestTarget()
            : $params['error_url'];
    }

    protected function updateUserSession(OidcAspect $aspect, bool $setMfa): void
    {
        if (ApplicationType::fromRequest($this->request)->isBackend()) {
            $user = $GLOBALS['BE_USER'];
        } elseif (isset($GLOBALS['TYPO3_REQUEST'])) {
            // The frontend.user attribute is added by the user authenticator
            // middleware that runs after this one. Hence, it does not exist
            // in $this->request, forcing us to examine the enriched global
            // object. This object is assigned by the final request handler,
            // which can be prevented by an intermediate middleware, e.g.
            // RedirectHandler.
            $user = $GLOBALS['TYPO3_REQUEST']->getAttribute('frontend.user');
        } else {
            $user = [];
        }

        // This also enforces that the user is logged in
        if ($user->user['oidc'] ?? false) {
            // Mark MFA as done if told so
            if ($setMfa) $user->setSessionData('mfa', true);
            // Save the oidc data
            GeneralUtility::makeInstance(UserStorage::class, $user)
                ->store($aspect, $user->user['oidc']);
        }
    }

    /**
     * Returns a unique ID for the current processed request.
     *
     * This is supposed to be independent of the actual web server (Nginx or Apache) and
     * the way PHP was built and unique enough for our use case, as opposed to using:
     *
     * - zend_thread_id() which requires PHP to be built with Zend Thread Safety - ZTS - support and debug mode
     * - apache_getenv('UNIQUE_ID') which requires Apache as web server and mod_unique_id
     *
     * @return string
     */
    static protected function getUniqueId(string $provider): string
    {
        return sprintf('%08x', abs(crc32($_SERVER['REMOTE_ADDR'] . $_SERVER['REMOTE_PORT'] . $_SERVER['REQUEST_TIME'] . $provider)));
    }

    /**
     * Adds query parameters to the provided url.
     *
     * Existing parameters with the same names are overwritten, while those with
     * different names are kept with the only exception of `cHash` - this parameter
     * is always removed, because otherwise typo3 rejects the request with an error.
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    static protected function withAddedQuery(string $url, array $params): string
    {
        $parts = parse_url($url);
        $url = explode('?', $url, 2)[0];

        $query = $parts['query'] ?? null;
        $fragment = $parts['fragment'] ?? null;

        if ($query) {
            parse_str($query, $queryParams);
            // We have to remove cHash when we modify the query string
            $params = array_merge(
                array_diff_key($queryParams, ['cHash' => 1]),
                $params
            );
        }

        if ($params) $url .= '?' . http_build_query($params);
        if ($fragment) $url .= '#' . $fragment;

        return $url;
    }

    static protected function asArray(mixed $value): array
    {
        return is_array($value) ? $value : [];
    }

    static protected function getLocationHeader(ResponseInterface $response): ?string
    {
        // Check sent headers first, because there is code that send headers
        // instead of passing the response object
        if (http_response_code() !== 200) {
            foreach (headers_list() as $header) {
                if (str_starts_with(strtolower($header), 'location:')) {
                    return ltrim(substr($header, 9));
                }
            }
        }
        return $response->getHeader('location')[0] ?? null;
    }
}
