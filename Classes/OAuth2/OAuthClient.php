<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\OAuth2;

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * OAuth client that communicates with an OpenID Connect provider:
 * - to authenticate a user and obtain an access token
 * - to fetch user resources
 * - to refresh or revoke a token
 *
 * This class should not be instantiated directly: use OAuthClientFactory
 * instead.
 *
 * @todo Document methods of this class
 */
class OAuthClient extends GenericProvider
{
    const GRANT_PASSWORD = 'password';
    const GRANT_AUTH_CODE = 'authorization_code';
    const GRANT_CLIENT = 'client_credentials';
    const GRANT_REFRESH_TOKEN = 'refresh_token';

    const TOKEN_TYPE_ACCESS = 'access_token';
    const TOKEN_TYPE_REFRESH = 'refresh_token';

    protected string $resourceOwnerIdClaim = 'sub';

    protected int $providerId = 0;

    protected string $providerName = '';

    protected ?string $mappedProperties = null;

    protected string $urlRevokeToken = '';

    protected function getConfigurableOptions(): array
    {
        return array_merge(parent::getConfigurableOptions(), [
            'providerId',
            'providerName',
            'resourceOwnerIdClaim',
            'mapping',
            'urlRevokeToken'
        ]);
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    public function getProviderName(): string
    {
        return $this->providerName;
    }

    /**
     * @throws IdentityProviderException
     */
    public function getAccessTokenFromCode(string $authorizationCode): AccessToken
    {
        return $this->getAccessToken(self::GRANT_AUTH_CODE, ['code' => $authorizationCode]);
    }

    /**
     * @throws IdentityProviderException
     */
    public function getAccessTokenForUser(string $username, string $password): AccessToken
    {
        return $this->getAccessToken(
            self::GRANT_PASSWORD, [
                'username' => $username,
                'password' => $password,
                'scope' => $this->getDefaultScopes()
            ]
        );
    }

    /**
     * @throws IdentityProviderException
     */
    public function getAccessTokenForClient(): AccessToken
    {
        return $this->getAccessToken(self::GRANT_CLIENT, ['scope' => $this->getDefaultScopes()]);
    }

    public function canRevokeTokens(): bool
    {
        return (bool)$this->urlRevokeToken;
    }

    public function revokeAccessToken(AccessToken $accessToken): bool
    {
        return $this->revokeToken($accessToken->getToken(), self::TOKEN_TYPE_ACCESS);
    }

    public function revokeRefreshToken(AccessToken $accessToken): bool
    {
        return $this->revokeToken($accessToken->getRefreshToken(), self::TOKEN_TYPE_REFRESH);
    }

    protected function revokeToken(string $token, string $tokenType): bool
    {
        // Do not treat as an error when the endpoint is not configured.
        // Nothing more can be done in such case.
        $endpointRevoke = $this->urlRevokeToken;
        if (!$endpointRevoke) return true;

        $encodedClientKey = base64_encode($this->clientId);
        $encodedClientSecret = base64_encode($this->clientSecret);

        $request = $this->getRequest(
            self::METHOD_POST,
            $endpointRevoke,
            [
                'headers' => [
                    'Authorization' => "Basic $encodedClientKey:$encodedClientSecret",
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => "token=$token&token_type_hint=$tokenType"
            ]
        );
        $response = $this->getResponse($request);
        $statusCode = $response->getStatusCode();

        if ($statusCode === 200) {
            return true;
        } else {
            /** @TODO more can be provided */
            return false;
        }
    }

    public function getRefreshedAccessToken(AccessToken $accessToken): AccessToken
    {
        return $this->getAccessToken(
            self::GRANT_REFRESH_TOKEN,
            ['refresh_token' => $accessToken->getRefreshToken()]
        );
    }

    protected function getIdToken(AccessToken $token): array
    {
        $idToken = $token->getValues()['id_token'] ?? null;
        if (empty($idToken)) return [];

        // $idToken is a JWT token with claims. As for now we will skip
        // the checks of the signature and simply decode the data
        try {
            $parts = explode('.', $idToken);
            $base64 = base64_decode(strtr($parts[1], '-_', '+/'));
            return \json_decode($base64, true);
        } catch (\Exception $e) {
            return [];
        }
    }

    protected function createResourceOwner(array $response, AccessToken $token): UserResource
    {
        $claims = array_merge($this->getIdToken($token), $response);
        $ownerId = $claims[$this->resourceOwnerIdClaim] ?? null;
        if (!$ownerId) {
            throw new IdentityProviderException(
                "resource owner id field '{$this->resourceOwnerIdClaim}' missing",
                1706714240,
                $claims
            );
        }
        return new UserResource(
            $claims,
            $this->providerId,
            $ownerId
        );
    }

}
