<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\OAuth2;

use TYPO3\CMS\Core\Http\RequestFactory;

class ProxyClient
{
    private string $secret;

    private string $baseUrl;

    protected RequestFactory $requestFactory;

    public function __construct(RequestFactory $requestFactory)
    {
        $this->requestFactory = $requestFactory;
    }

    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    public function setBaseUrl(string $path): void
    {
        $this->baseUrl = $path;
    }

    /**
     * Registers with a proxy a forward for a future callback.
     */
    public function registerForward(string $initial, ?string $callback = null): array
    {
        $body = ['initial' => $initial];
        if ($callback) $body['callback'] = $callback;
        return $this->request('', 'POST', $body);
    }

    /**
     * Deletes from a proxy a forward of a callback.
     */
    public function cancelForward(string $state): array
    {
        return $this->request("/$state", 'DELETE');
    }

    protected function request(string $path, string $method, ?array $body = null): array
    {
        $options = $this->getHttpOptions();
        if ($body) $options['form_params'] = $body;

        $response = $this->requestFactory->request(
            $this->baseUrl . $path, $method, $options
        );
        return \json_decode($response->getBody()->getContents(), true) ?: [
            'success' => false,
            'error' => 'internal server error'
        ];
    }

    protected function getHttpOptions(): array
    {
        return [
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Authorization' => $this->secret
            ],
            'allow_redirects' => false
        ];
    }

}
