<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\OAuth2;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * The resource owner returned by an oidc provider that can be converted
 * to a typo3 user. This class implements mapping typo3 properties using
 * claims from the resource according to rules specified in the configuration
 * of the oidc provider.
 *
 * @todo document methods
 */
class UserResource implements ResourceOwnerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected array $claims;

    protected int $providerId;

    protected string $resourceOwnerId;

    public function __construct(
        array $claims,
        int $providerId,
        string $resourceOwnerId
    ) {
        $this->providerId = $providerId;
        $this->resourceOwnerId = $resourceOwnerId;
        $this->claims = $claims;
    }


    public function getId(): string
    {
        return $this->resourceOwnerId;
    }

    public function toArray(): array
    {
        return [
            'claims' => $this->claims,
            'provider' => $this->providerId
        ];
    }

    public function getProviderId(): int
    {
        return $this->providerId;
    }

    /**
     * Returns the value of the requested claim or null if not specified.
     *
     * @param string $claim
     * @return mixed|null
     */
    public function get(string $claim)
    {
        return $this->claims[$claim] ?? null;
    }

    /**
     * Sets the value of a claim or removes the claim if the value is null.
     *
     * @param string $claim
     * @param mixed $value
     * @return void
     */
    public function set(string $claim, mixed $value): void
    {
        if (!is_null($value)) {
            $this->claims[$claim] = $value;
        } elseif (isset($this->claims[$claim])) {
            unset($this->claims[$claim]);
        }
    }

    public function getClaims(): array
    {
        return $this->claims;
    }

    public function setClaims(array $claims): void
    {
        $this->claims = $claims;
    }
}
