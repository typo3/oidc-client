<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Service;

/**
 * Authentication related settings of the extension, either for
 * the frontend or the backend application. This class returns
 * default values if a requested setting is not specified.
 */
class AuthenticationSettings
{
    /** @var array The array with settings */
    protected array $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Indicates whether users can be authenticated by this extension.
     * @return bool
     */
    public function enabled(): bool
    {
        return (bool)($this->settings['enableAuthentication'] ?? false);
    }

    /**
     * Indicates whether deleted users can be undeleted after a successful authentication.
     * @return bool
     */
    public function userRecycle(): bool
    {
        return (bool)($this->settings['recycleUsers'] ?? false);
    }

    /**
     * The default URL to which a user is redirected after a successful authentication.
     * @return string
     */
    public function redirectUrl(): string
    {
        return $this->settings['redirectUrl'] ?? '';
    }
}
