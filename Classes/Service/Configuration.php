<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Service;

use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The provider of extension settings. Takes care of value types
 * and the settings and returns default values if a setting is
 * no specified.
 *
 * This class implements a singleton patterns: an instance is
 * obtained by calling the the static method getInstance(), which
 * creates only one instance of this class, initialized with the
 * settings of the extension.
 */
class Configuration
{
    /** @var array The array with extension setting */
    protected array $settings;

    /** @var self An instance of this class */
    private static ?self $instance = null;

    /**
     * Returns an instance of this class initialized with the extension
     * settings. This is the standard way to instantiate this class.
     */
    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new self(
                GeneralUtility::makeInstance(ExtensionConfiguration::class)
                ->get(Constants::EXT_KEY) ?? []
            );
        }
        return self::$instance;
    }

    // Make the constructor a protected method so that it is not
    // called directly, and yet it is accessible from a derived
    // class, allowing to choose custom settings in tests.
    protected function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /***********************************************************/
    /*                                                         */
    /*   Basic oauth settings                                  */
    /*                                                         */
    /***********************************************************/

    /**
     * The default name of the cookie with session id
     * @return string
     */
    public function sessionCookieName(): string
    {
        return @$this->settings['sessionCookieName'] ?: Constants::DEFAULT_SESSION_COOKIE;
    }

    /**
     * The default path of the cookie with session id
     * @return string
     */
    public function sessionCookiePath(): string
    {
        return @$this->settings['sessionCookiePath'] ?: '/';
    }

    /**
     * The default scopes sent with an authentication request
     * @return string[]
     */
    public function clientScopes(): array
    {
        return GeneralUtility::trimExplode(',', $this->settings['defaultScopes'], true) ?: ['opendid'];
    }

    /**
     * The default callback URL that is sent to OIDC providers.
     * @return string
     */
    public function callbackUrl($host = null): string
    {
        $callbacks = explode(" ", $this->settings['callbackUri'] ?? '');
        // If a host is provided then make sure it ends with a slash
        // so that the path is matched properly
        if ($host && !str_ends_with($host, '/')) $host .= '/';
        $noDomain = null;
        // Return the first full URL if no domain is provided. Otherwise, find one
        // matching the provided host or prepend the domain to the first callback
        // with no domain specified.
        foreach ($callbacks as $url) {
            if (str_starts_with($url, "/")) {
                if (empty($noDomain)) $noDomain = $host . substr($url, 1);
            } elseif (!$host || str_starts_with($url, $host)) {
                return $url;
            }
        }
        return $noDomain ?? ($host . substr($this->oidcRouteBase(), 1) . Constants::ROUTE_CALLBACK);
    }

    /**
     * The base path of request processed by this extension.
     * The returned string is guaranteed to start and end with a slash '/'.
     * @return string
     */
    public function oidcRouteBase(): string
    {
        $path = $this->settings['oidcRouteBase'] ?? null;
        if (!$path) {
            return Constants::ROUTE_BASE;
        }
        if (!str_ends_with($path, '/')) $path .= '/';
        if (!str_starts_with($path, '/')) $path = '/' . $path;
        return $path;
    }

    /**
     * The secret key for validating tokens with request parameters
     * that must be protected to increase site security
     */
    public function securityPublicKey(): string
    {
        return $this->settings['secretKey'];
    }

    /**
     * The secret key for signing tokens with request parameters
     * that must be protected to increase site security
     */
    public function securityPrivateKey(): string
    {
        return $this->settings['secretKey'];
    }

    /**
     * The signing algorithm for tokens
     */
    public function securityTokenAlg(): string
    {
        return 'HS256';
    }

    /***********************************************************/
    /*                                                         */
    /*   Callback forward service configuration                */
    /*                                                         */
    /***********************************************************/

    /**
     * The endpoint for the REST API of the callback forwarding service 
     */
    public function forwardServiceUrl(): string
    {
        return $this->settings['forwardServiceEndpoint'] ?? '';
    }

    /**
     * The secret token to authenticate calls the callback forwarding service 
     */
    public function forwardServiceSecret(): string
    {
        return $this->settings['forwardServiceSecret'] ?? '';
    }

    /***********************************************************/
    /*                                                         */
    /*   Advanced oauth service settings                       */
    /*                                                         */
    /***********************************************************/

    /**
     * The desired priority of the authentication service
     */
    public function authServicePriority(): int
    {
        return (int)($this->settings['authenticationServicePriority'] ?? 82);
    }

    /**
     * The desired quality of the authentication service
     */
    public function authServiceQuality(): int
    {
        return (int)($this->settings['authenticationServiceQuality'] ?? 80);
    }

    /**
     * Indicates whether the access token must be revoked after the authentication process.
     * @return bool
     */
    public function revokeAccessToken(): bool
    {
        return (bool)($this->settings['revokeAccessTokenAfterLogin'] ?? false);
    }

    /**
     * Indicates whether the code verifier should be used during the authenticsation process.
     * @return bool
     */
    public function enableCodeVerifier(): bool
    {
        return (bool)($this->settings['enableCodeVerifier'] ?? false);
    }

    /**
     * Indicates whether possible CSRF attacks should be ignored.
     * @return bool
     */
    public function disableCsrfProtection(): bool
    {
        return (bool)($this->settings['disableCSRFProtection'] ?? false);
    }

    /***********************************************************/
    /*                                                         */
    /*   User authentication settings                          */
    /*                                                         */
    /***********************************************************/

    public function frontend(): AuthenticationSettings
    {
        return new AuthenticationSettings($this->settings['frontend'] ?? []);
    }

    public function backend(): AuthenticationSettings
    {
        return new AuthenticationSettings($this->settings['backend'] ?? []);
    }

}
