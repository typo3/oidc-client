<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Service;

use Doctrine\DBAL\Driver\Exception as DBALExceptionBase;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\OAuth2\UserResource;
use IMATHUZH\OidcClient\Session\OidcSession;
use IMATHUZH\OidcClient\Domain\Repository\OidcRepository;
use IMATHUZH\OidcClient\Event\ModifyResourceOwnerEvent;
use IMATHUZH\OidcClient\Exception\OAuthException;
use IMATHUZH\OidcClient\OAuth2\ProxyClient;
use IMATHUZH\OidcClient\OAuth2\OAuthClient;
use IMATHUZH\OidcClient\Session\OidcSessionManager;
use IMATHUZH\OidcClient\Utility\Constants;
use IMATHUZH\OidcClient\Utility\ExceptionThrowTrait;
use IMATHUZH\OidcClient\Utility\FlexFormUtility;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A collection of methods related to processing OpenID Connect requests
 */
class OAuthService implements LoggerAwareInterface, SingletonInterface
{
    use LoggerAwareTrait;
    use ExceptionThrowTrait;

    /**
     * The number of seconds by which the token validity is extended when
     * compared to the iat and exp claims of the identity token. The main
     * purpose of this value is to treat clock skews gracefully.
     */
    const TOKEN_TIME_TOLERANCE = 300;

    protected Configuration $settings;

    protected OidcRepository $oidcRepository;

    protected OidcSessionManager $sessionManager;

    protected OidcSession $session;

    protected array $providerConfig;

    protected OAuthClient $client;


    public function __construct(
        Configuration $settings,
        OidcSessionManager $sessionManager,
        OidcRepository $oidcRepository
    ) {
        $this->settings = $settings;
        $this->sessionManager = $sessionManager;
        $this->oidcRepository = $oidcRepository;
    }

    /*************************************************************/
    /*  Authentication process flow controllers                  */
    /*    initialize() starts a new authentication process       */
    /*    persist() persists the state of the current process,   */
    /*          so that it can be continued in a next request    */
    /*    restore() loads a previously saved state or a process  */
    /*    tearDown() deletes persistent data associated with     */
    /*          the currnent process                             */
    /*************************************************************/

    /**
     * Reloads the configuration of an already existing authentication process
     * with the given request id or initiates a new one.
     * 
     * An existing configuration is validated for the provider used as well as
     * the secret token stored in a cookie. If a new process is created, then
     * this method registers a callback forward service if the requested provider
     * is configured to use one.
     */
    public function initialize(string|int $provider, string $requestId, string $callbackHost): bool
    {
        $session = $this->sessionManager->loadSession($requestId);
        if (!$session) {
            // There is currently no process for this id. Hence, we create
            // a new one by building the authorization url and save parameters
            // in an oidc session.
            $this->loadProviderConfiguration($provider);
            $callback = $this->settings->callbackUrl($callbackHost);
            if ($this->providerConfig['callback_forwarding']) {
                $response = $this->requestCallbackForward($requestId, $callback);
                $this->providerConfig['callback_url'] = $response['callback'];
                $state = $response['state'];
            } else {
                $state = self::getRandomState($requestId);
                $this->providerConfig['callback_url'] = $callback;
            }
            $this->session = $this->sessionManager->createSession($state);
            // Save the callback URL, because some provider, like Switch Edu-Id,
            // require it to provider the access token or user resource
            $this->session->callback_url = $this->providerConfig['callback_url'];
            return false;
        }

        // Validate the existing session by examining the session token and provider.
        // In a normal usage both tests are satisfied.
        if ($session->getToken() !== $this->getSessionToken($requestId)) {
            $this->logger->warning("failed to load an existing session $requestId: token mismatch");
            throw new OAuthException("invalid state - oidc session mismatch");
        }
        if ($session->provider !== $provider) {
            $this->logger->warning("failed to load an existing session $requestId: provider mismatch");
            throw new OAuthException("invalid state - oidc provider mismatch");
        }
        
        $this->session = $session;
        return true;
    }

    /**
     * Loads the configuration of the given provider from the database.
     * 
     * @param string|int $provider   The uid or name of the provider
     * @throws OAuthException    no matching provider is found
     */
    protected function loadProviderConfiguration(string|int $provider): void
    {
        $this->providerConfig = $this->oidcRepository->findProviderConfig($provider)
            ?: self::raise(new OAuthException("unknown OpenID Connect provider '$provider'", 1707816385));
    }

    /**
     * Saves the service state in a database and returns the cookie header for
     * the response header, so that this state can be restored when processing
     * following requests.
     */
    public function persist(): string
    {
        $this->sessionManager->saveSession($this->session);
        // from session: id and token
        $cookieName = $this->settings->sessionCookieName() . $this->getRequestId();
        $cookieValue = $this->session->getToken();
        $cookiePath = $this->settings->sessionCookiePath();
        return "$cookieName=$cookieValue; HttpOnly; SameSite=Lax; Path=$cookiePath";
    }

    /**
     * Loads the parameters of an authentication process associated with the given state.
     */
    public function restore(string $state): void
    {
        $session = $this->sessionManager->loadSessionWithState($state);
        if (!$session) {
            $this->logger->error("no session exists for state $state");
            throw new OAuthException("invalid state - no session found");
        }
        $ownToken = $this->getSessionToken($state);
        if ($session->getToken() !== $this->getSessionToken($state)) {
            $this->logger->error("cannot restore process $state: invalid token", [
                'owned' => $ownToken,
                'required' => $session->getToken()
            ]);
            throw new OAuthException("invalid state - token mismatch");
        }

        $this->logger->debug("Loaded session: ", $session->getData());

        $this->session = $session;
        $this->loadProviderConfiguration($this->session->provider);
        // Restore the callback URL
        $this->providerConfig['callback_url'] = $this->session->callback_url;
    }

    /**
     * Deletes the service state from the database and returns the cookie header
     * for the response that will ask to delete the session cookie.
     */
    public function tearDown(): string
    {
        // delete session
        $this->sessionManager->deleteSession($this->session);
        // cancel the callback forward if registered
        if ($this->providerConfig['callback_forwarding']) {
            $this->cancelCallbackForward($this->getState());
        }
        // return the cookie header string
        $cookieName = $this->settings->sessionCookieName() . $this->getRequestId();
        return "$cookieName=delete; Max-Age=0; expires="
            . gmdate('D, d-M-Y H:i:s T', $GLOBALS['EXEC_TIME'] - 31536001);
    }


    public function getAspect(): OidcAspect
    {
        $context = GeneralUtility::makeInstance(Context::class);
        if ($context->hasAspect(Constants::ASPECT)) {
            return $context->getAspect(Constants::ASPECT);
        }
        if (!isset($this->session->_aspect)) {
            $aspect = new OidcAspect(
                $this->providerConfig,
                $this->session->getData(),
                $this->providerConfig[$this->session->request_type] ?? 0
            );
            $this->session->_aspect = $aspect;
        } else {
            $aspect = $this->session->_aspect;
        }
        GeneralUtility::makeInstance(Context::class)
            ->setAspect(Constants::ASPECT, $aspect);
        return $aspect;
    }


    /*************************************************************/
    /*                                                           */
    /*     Accessors for data and properties of the process      */
    /*                                                           */
    /*************************************************************/

    /**
     * The state of the current process. Requires an object initialized
     * by {@see initialize()} or {@see restore()}.
     */
    public function getState(): string
    {
        return $this->session->getIdentifier();
    }

    /**
     * The id of the current process. Requires an object initialized
     * by {@see initialize()} or {@see restore()}.
     */
    public function getRequestId(): string
    {
        return substr($this->session->getIdentifier(), 0, 8);
    }

    public function getProviderId(): int
    {
        return $this->providerConfig['uid'] ?? 0;
    }

    public function hasMfa(): bool
    {
        return (bool)($this->providerConfig['mfa'] ?? false);
    }

    public function isEnabledForFrontend(): bool
    {
        return (bool)($this->providerConfig['frontend'] & Constants::AUTH_ENABLED);
    }

    public function isEnabledForBackend(): bool
    {
        return (bool)($this->providerConfig['backend'] & Constants::AUTH_ENABLED);
    }

    public function isEnabled(): bool
    {
        $requestType = $this->session->request_type;
        return (bool)($this->providerConfig[$requestType] & Constants::AUTH_ENABLED);
    }

    /**
     * Gets the value of a key saved in a persistent storage.
     * 
     * Requires an object initialized by {@see initialize()}
     * or {@see restore()}.
     */
    public function get(string $key): mixed
    {
        return $this->session->$key;
    }

    /**
     * Sets the value in a persistent storage.
     * 
     * The updated value can be retrieved in subsequent HTTP requests
     * once {@see persist()} is called. This method requires an object
     * initialized by {@see initialize()} or {@see restore()}.
     */
    public function set(string $key, mixed $value): void
    {
        $this->session->$key = $value;
    }

    /**
     * Adds values to a persistent storage.
     * 
     * This method overwrites existing values for the given keys,
     * but values with keys that are not present in $parameters
     * are left unchanged. This method requires an object
     * initialized by {@see initialize()} or {@see restore()}.
     */
    public function setParameters(array $parameters): void
    {
        $this->session->addData($parameters);
    }

    /**
     * Fetches the configuration of the current provider from the database.
     *
     * @throws OAuthException if the configuration is not found
     * @throws DBALExceptionBase
     * @deprecated the configuration should no longer be obtained from this class
     */
    public function getProviderConfiguration(): array
    {
        if (!isset($this->providerConfig)) {
            $providerId = $this->session->provider ?? 0;
            $this->providerConfig = $this->oidcRepository->findProviderConfig($providerId)
                ?: self::raise(new OAuthException("unknown OpenID Connect provider '$providerId'", 1707816385));
        }
        return $this->providerConfig;
    }


    /*************************************************************/
    /*                                                           */
    /*  Authentication service methods                           */
    /*                                                           */
    /*************************************************************/

    /**
     * Prepares parameters for the authorization endpoint of the provider
     * and returns the authorization url. The parameters must be preserved
     * by calling {@see persist()} and adding the cookie returned by this
     * method to the response.
     */
    public function buildAuthorizationUrl(array $options = []): string
    {
        // Create provider and ask for the authorization url
        // $oauth = new OAuthClient($this->providerConfig);
        $oauth = $this->createClient();
        $options['state'] = $this->session->getIdentifier();
        $this->session->auth_url = $oauth->getAuthorizationUrl($options);
        // Save the code verifier if used
        if ($this->settings->enableCodeVerifier()) {
            $this->session->code_verifier = $oauth->getPkceCode();
        }
        return $this->session->auth_url;
    }

    public function getAuthorizationUrl(): string
    {
        return $this->session->auth_url ?? '';
    }
    
    /**
     * Registers a callback forwarding service on a proxy server
     */
    public function requestCallbackForward(string $requestId, ?string $callback = null): array
    {
        $proxy = GeneralUtility::makeInstance(ProxyClient::class);
        $proxy->setSecret($this->settings->forwardServiceSecret());
        $proxy->setBaseUrl($this->settings->forwardServiceUrl());
        $result = $proxy->registerForward($requestId, $callback);
        if (!$result['success']) {
            $this->logger->error('Failed to register a callback forward', $result);
            throw new \RuntimeException("service unavailable", 1708371533);
        }
        return $result;
    }

    /**
     * Cancels a callback forwarding service on a proxy server
     */
    public function cancelCallbackForward(string $state): void
    {
        $proxy = GeneralUtility::makeInstance(ProxyClient::class);
        $proxy->setSecret($this->settings->forwardServiceSecret());
        $proxy->setBaseUrl($this->settings->forwardServiceUrl());
        $result = $proxy->cancelForward($state);
        if (!$result['success']) {
            $this->logger->warning('Failed to cancel a callback forward', $result);
        }
    }

    /**
     * 
     */
    public function processAuthorizationCode(string $code): UserResource // UserResource
    {
    //    $oauth = new OAuthClient($this->providerConfig);
        $oauth = $this->createClient();
        if ($this->settings->enableCodeVerifier()) {
            $oauth->setPkceCode($this->session->code_verifier ?? '');
        }
        $accessToken = $oauth->getAccessTokenFromCode($code);
        /** @var UserResource $resource */
        $resource = $oauth->getResourceOwner($accessToken);

        $requestType = $this->get('request_type') ?? 'frontend';
        $eventDispatcher = GeneralUtility::makeInstance(EventDispatcherInterface::class);
        $event = new ModifyResourceOwnerEvent(
            $resource, $accessToken, $this->getAspect(), $requestType[0] . 'e',
            FlexFormUtility::flexData2array($this->providerConfig['processors'])
        );
        $eventDispatcher->dispatch($event);

        // revoke the access token is configured so
        if ($this->settings->revokeAccessToken()) {
            try {
                $oauth->revokeAccessToken($accessToken);
            } catch (\Exception $e) {
                $this->logger->warning('Could not revoke the access token', [
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $resource;
    }

    public function processIdToken(AccessTokenInterface $token, int $atTime = 0): void
    {
        $idToken = $token->getValues()['id_token'] ?? null;
        if ($idToken) {
            /** @todo verify the signature of the token */
            $parts = explode('.', $idToken, 3);
            $data = base64_decode(strtr($parts[1], '-_', '+/'));

            // Check validity time
            if (empty($data['iat']) || $data['iat'] > $atTime + self::TOKEN_TIME_TOLERANCE)
                throw new OAuthException("Invalid token: premature access", 1707918411);
            if (empty($data['exp']) || $data['exp'] < $atTime - self::TOKEN_TIME_TOLERANCE)
                throw new OAuthException("the ID token has expired", 1707918412);
            // Check the issuer
            if (empty($data['iss']) || (
                $this->providerConfig['issuer'] ? ($this->providerConfig['issuer'] !== $data['iss'])
                     : !str_starts_with($this->providerConfig['auth_endpoint'], $data['iss'])
            ))
                throw new OAuthException("invalid ID token - issuer mismatch", 1707918413);
            // Check the audience
            if (empty($data['aud']) || $data['aud'] !== $this->providerConfig['client_id'])
                throw new OAuthException("invalid ID token - audience mismatch", 1707918414);
        }
    }

    /**
     * @todo improve: extend AbstractProvider instead of GenericProvider,
     * so that no factory method is necessary
     */
    protected function createClient(): OAuthClient
    {
        $configuration = $this->providerConfig;
        return new OAuthClient([
            'resourceOwnerIdClaim' => $configuration['id_claim'] ?: 'sub',
            'providerId' => $configuration['uid'],
            'providerName' => $configuration['name'],
            'clientId' => $configuration['client_id'],
            'clientSecret' => $configuration['client_secret'],
            'redirectUri' => ($configuration['callback_url'] ?? null) ?: $this->settings->callbackUrl(),
            'urlAuthorize' => $configuration['auth_endpoint'],
            'urlAccessToken' => $configuration['token_endpoint'],
            'urlRevokeToken' => $configuration['revoke_endpoint'],
            'urlResourceOwnerDetails' => $configuration['userinfo_endpoint'],
            'scopes' => $configuration['client_scopes'] ?: $this->settings->clientScopes(),
            'mappedProperties' => $configuration['mapping'],
            'pkceMethod' => $this->settings->enableCodeVerifier() ? OAuthClient::PKCE_METHOD_S256 : null
        ]);
    }

/*******************************************************************************/

    protected function getSessionToken(string $requestIdOrState): string
    {
        $cookieName = $this->settings->sessionCookieName() . substr($requestIdOrState, 0, 8);
        return $_COOKIE[$cookieName] ?? '';
    }

    /**
     * Generates a random state with the given initial string
     */
    protected static function getRandomState(string $beginsWith = ''): string
    {
        return substr($beginsWith . bin2hex(random_bytes(16)), 32);
    }

    /**
     * Encodes the provided array into a JWT, signed with the key from the extension settings.
     *
     * @param array $settings
     * @return string
     */
    public function encodeSettings(array $settings): string
    {
        return JWT::encode(
            array_merge($settings, [
                'iat' => $GLOBALS['EXEC_TIME'],
                'nbf' => $GLOBALS['EXEC_TIME'],
                'exp' => $GLOBALS['EXEC_TIME'] + 84600*7
            ]),
            $this->settings->securityPrivateKey(),
            $this->settings->securityTokenAlg()
        );
    }

    /**
     * Validates the signature of the provided JWT using the key
     * from the extension settings and decodes into an array.
     *
     * @param string|null $encoded
     * @return array
     */
    public function decodeSettings(?string $encoded): array
    {
        if (empty($encoded)) return [];
        try {
            $key = new Key(
                $this->settings->securityPublicKey(),
                $this->settings->securityTokenAlg()
            );
            return (array)JWT::decode($encoded, $key);
        } catch (\Exception $e) {
            $this->logger->error("Failed to decode a security token: " . $e->getMessage());
            return [];
        }
    }
}
