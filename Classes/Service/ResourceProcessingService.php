<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Service;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\SingletonInterface;
use IMATHUZH\OidcClient\Utility\ExceptionThrowTrait;

/**
 * @todo This is just a stub
 */
class ResourceProcessingService implements LoggerAwareInterface, SingletonInterface
{
    use LoggerAwareTrait;
    use ExceptionThrowTrait;

    
    /**
     * Returns 
     */
    public function getOidcAccount(int $providerId, string $sub): array
    {

    }

    public function findMatchingUser(array $resource): array
    {

    }

    public function convertResourceToTypo3User(array $resource): array
    {
        
    }
}
