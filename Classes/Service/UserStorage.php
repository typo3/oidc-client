<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Service;

use IMATHUZH\OidcClient\Context\OidcAspect;
use IMATHUZH\OidcClient\Utility\Constants;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Authentication\AbstractUserAuthentication;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A class for persisting and accessing oidc data for recently
 * used accounts associated with the current user.
 */
class UserStorage
{
    protected static array $claimsToStore = [
        'sub', 'given_name', 'family_name', 'email', 'email_verified'
    ];

    public function __construct(
        protected ?AbstractUserAuthentication $user = null
    ) { }

    /**
     * Stores data from the aspect in the session of the current user
     * under with provided index. This data is also made available
     * under `recent` key.
     *
     * @param OidcAspect $aspect
     * @param int $uid
     * @return void
     */
    public function store(OidcAspect $aspect, int $uid): void
    {
        $user = $this->getUser();
        $resource = $this->convertAspectToResource($aspect);
        $oidcData = $user->getSessionData('oidc') ?? [];
        $oidcData[$uid] = &$resource;
        $oidcData['recent'] = &$resource;
        $user->setAndSaveSessionData('oidc', $oidcData);
    }

    /**
     * Creates a dictionary of oidc data from a given aspect.
     * The resulting array have the following keys:
     * + `resource_id` - the id of the oidc resource
     * + `provider` - the name of the oidc provider
     * + `auth_time` - the time the authentication took place
     * Keys are listed in {@see self::$claimToStore}
     * 
     * @param OidcAspect $aspect
     * @return array
     */
    protected function convertAspectToResource(OidcAspect $aspect): array
    {
        return $aspect->isAuthenticated() ? array_merge(
            array_intersect_key(
                $aspect->getResource()->getClaims(),
                array_flip(self::$claimsToStore)
            ), [
                'resource_id' => $aspect->getResourceId(),
                'provider' => $aspect->getProviderName(),
                'auth_time' => $GLOBALS['EXEC_TIME']
            ]
        ) : [];
    }

    /**
     * Deletes the oidc data with the provided key.
     * If this record is pointed by `recent`, then this key
     * is also cleared.
     *
     * @param int $uid
     * @return void
     */
    public function delete(int $uid): void
    {
        $user = $this->getUser();
        $data = $user->getSessionData('oidc') ?? [];
        if (isset($data[$uid])) {
            if ($data[$uid] === $data['recent']) {
                $data['recent'] = null;
            }
            unset($data[$uid]);
            $user->setAndSaveSessionData('oidc', $data);
        }
    }

    /**
     * Fetches the oidc data for the provided index.
     * If the index is 0 or omitted, then the most
     * recent data is returned.
     *
     * @param int $uid
     * @return array|null
     */
    public function get(int $uid=0): ?array
    {
        $data = $this->getUser()->getSessionData('oidc') ?? [];
        if ($data && $uid && $uid !== count($data)) {
            return $data[$uid] ?? null;
        }
        // $uid = 0 means the recent data, which might have been just updated
        // $uid = count($uid) might point to the current data that is not saved yet
        $aspectData = $this->getAspectData();
        return $uid ? $aspectData : ($aspectData ?: ($data['recent'] ?? null));
    }

    private function getAspectData(): ?array
    {
        try {
            return $this->convertAspectToResource(
                GeneralUtility::makeInstance(Context::class)
                    ->getAspect(Constants::ASPECT)
            );
        } catch (AspectNotFoundException) {
            return null;
        }
    }

    /**
     * Converts the entire store to an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getUser()->getSessionData('oidc') ?? [];
    }

    /**
     * A helper method that returns the current request
     *
     * @return ServerRequestInterface
     */
    protected function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }

    /**
     * The currently logged-in user or null.
     *
     * @return AbstractUserAuthentication|null
     */
    protected function getUser(): ?AbstractUserAuthentication
    {
        if (!$this->user) {
            $request = $this->getRequest();
            $this->user = ApplicationType::fromRequest($request)->isFrontend()
                ? $request->getAttribute('frontend.user')
                : $GLOBALS['BE_USER'];
        }
        return $this->user;
    }
}