<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Session;

/**
 * An interface to the oidc session loaded by the session manager.
 */
class OidcSession
{
    protected string $_id;
    protected string $_token;
    protected array $_data;

    public function __construct(string $id, string $token, array $data = [])
    {
        $this->_id = $id;
        $this->_token = $token;
        $this->_data = $data;
    }

    public function getIdentifier(): string
    {
        return $this->_id;
    }

    public function getToken(): string
    {
        return $this->_token;
    }

    public function getData(): array
    {
        return $this->_data;
    }

    public function setData(array $data = []): void
    {
        $this->_data = $data;
    }

    public function addData(array $data = []): void
    {
        $this->_data = array_merge($this->_data, $data);
    }

    // Generic accessors

    public function __get(string $key): mixed
    {
        return $this->_data[$key] ?? null;
    }

    public function __set(string $key, mixed $value): void
    {
        if (is_null($value)) {
            if (isset($this->_data[$key])) {
                unset($this->_data[$key]);
            }
        } else {
            $this->_data[$key] = $value;
        }
    }

    public function __isset(string $key): bool
    {
        return isset($this->_data[$key]);
    }

    public function __unset(string $key): void
    {
        if (isset($this->_data[$key])) {
            unset($this->_data[$key]);
        }
    }
}
