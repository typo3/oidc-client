<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Session;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception as DBALExceptionBase;
use IMATHUZH\OidcClient\Utility\Constants;
use IMATHUZH\OidcClient\Utility\ExceptionThrowTrait;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Sessions are identified by the oidc state and secured with a secret token.
 * The token can be stored in a cookie - this will allow only one simultaneous
 * authentication process, which should not be a big restriction.
 */
class OidcSessionManager
{
    use ExceptionThrowTrait;

    /** The size of the session token */
    const SESSION_TOKEN_SIZE = 32;      // the size of the token field in DB

    /** The name of the table with session data */
    const TABLE_NAME = Constants::TABLE_SESSION;

    /** The timestamp of the oldest valid session */
    protected int $lastValidTstamp;

    /** A connection to the database for the table with session data */
    protected Connection $connection;


    public function __construct(int $sessionLifeTime = 15*60)
    {
        $this->lastValidTstamp = $GLOBALS['EXEC_TIME'] - $sessionLifeTime;
        $this->connection = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable(self::TABLE_NAME);
    }

    /**
     * Generates a random token
     *
     * @return string
     * @throws \Exception   from {@see random_bytes()}
     */
    protected function getRandomToken(): string
    {
        return bin2hex(random_bytes(self::SESSION_TOKEN_SIZE >> 1));
    }

    /**
     * Creates a session object from a row fetched from a database.
     *
     * @param array $row
     * @return OidcSession
     */
    public function createFromRecord(array $row): OidcSession
    {
        return new OidcSession($row['state'], $row['token'], unserialize($row['data']));
    }

    /**
     * Creates a new session object with a provided state and token.
     * A random token is generated if none is provided
     *
     * @param string $state
     * @param string $token
     * @return OidcSession
     * @throws \Exception    from {@link random_bytes()}
     */
    public function createSession(string $state, string $token = ''): OidcSession
    {
        return new OidcSession($state, $token ?: $this->getRandomToken());
    }

    /**
     * Loads a session object with a matching value of the provided field.
     * Returns `null` is no session is found.
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @return OidcSession|null
     * @throws DBALException
     * @throws DBALExceptionBase
     */
    protected function loadSessionByField(string $fieldName, string $fieldValue): ?OidcSession
    {
        $query = $this->connection->createQueryBuilder();
        $expr = $query->expr();
        $session = $query->select('state', 'token', 'data')
            ->from(self::TABLE_NAME)
            ->where(
                $expr->eq($fieldName, $query->quote($fieldValue)),
                $expr->gt('tstamp', $this->lastValidTstamp)
            )
            ->executeQuery()
            ->fetchAssociative();
        return $session ? $this->createFromRecord($session)
                        : null;
    }

    /**
     * Loads an unexpired session from the database that matches
     * the given session id or returns null otherwise.
     *
     * @param string $sesid
     * @return OidcSession|null
     * @throws DBALExceptionBase|DBALException  on database error
     */
    public function loadSession(string $sesid): ?OidcSession
    {
        return $this->loadSessionByField('sesid', $sesid);
    }

    /**
     * Loads a session from the database that matches the given
     * state or returns null otherwise.
     *
     * @throws DBALExceptionBase|DBALException  on database error
     */
    public function loadSessionWithState(string $state): ?OidcSession
    {
        return $this->loadSessionByField('state', $state);
    }

    /**
     * Saves a session in the backend. This creates a token that
     * must be added to a cookie. Hence, this method should be
     * followed by a call to updateSessionCookie() on the response
     * object.
     *
     * @return bool
     */
    public function saveSession(OidcSession $session): bool
    {
        $sesid = substr($session->getIdentifier(), 0, 8);
        $data = serialize($session->getData());
        // Try to insert the record first. This will fail if
        // a session with the given sesid already exists.
        try {
            $inserted = $this->connection->insert(self::TABLE_NAME, [
                'sesid' => $sesid,
                'state' => $session->getIdentifier(),
                'token' => $session->getToken(),
                'tstamp' => $GLOBALS['EXEC_TIME'],
                'data' => $data
            ]);
            if ($inserted > 0) return true;
        } catch (\Exception $e) {}
        // Update an existing persisted session only if it is expired
        // or has the same state
        try {
            $query = $this->connection->createQueryBuilder();
            $expr = $query->expr();
            $updated = $query->update(self::TABLE_NAME)
                  ->set('data', $data)
                  ->set('state', $session->getIdentifier())
                  ->set('token', $session->getToken())
                  ->set('tstamp', $GLOBALS['EXEC_TIME'])
                  ->where($expr->or(
                      $expr->eq('state', $query->quote($session->getIdentifier())),
                      $expr->and(
                          $expr->eq('sesid', $query->quote($sesid)),
                          $expr->lt('tstamp', $this->lastValidTstamp)
                      )
                  ))->executeStatement();
            return $updated > 0;
        } catch (\Exception $e) { }
        // Update has failed - there is another session with the same id.
        // This is very unlikely.
        return false;
    }

    /**
     * Removes a session with the given identifier.
     */
    public function deleteSessionById(string $sesid): void
    {
        $this->connection->delete(self::TABLE_NAME, ['sesid' => $sesid]);
    }

    public function deleteSessionByState(string $state): void
    {
        $this->connection->delete(self::TABLE_NAME, ['state' => $state]);
    }

    public function deleteSession(OidcSession $session): void
    {
        $this->connection->delete(self::TABLE_NAME, ['state' => $session->getIdentifier()]);
    }

    /**
     * Deletes all expired sessions and returns the number
     * of sessions found.
     */
    public function deleteExpiredSessions(): int
    {
        $query = $this->connection->createQueryBuilder();
        return $query->delete(self::TABLE_NAME)
            ->where($query->expr()->lt('tstamp', $this->lastValidTstamp))
            ->executeStatement();
    }

}
