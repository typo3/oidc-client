<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

/**
 * Constants with possible values returned by an authentication service.
 *
 * This class is used to set returned values, but it should not be used for checking
 * values: SUCCESS_BREAK and FAILURE_CONTINUE states are represented by ranges of values,
 * whereas this class provides only specific values.
 */
class AuthenticationStatus
{
    /**
     * true - this service was able to authenticate the user
     */
    const SUCCESS_CONTINUE = true;

    /**
     * 200 - authenticated and no more checking needed
     */
    const SUCCESS_BREAK = 200;

    /**
     * false - this service was the right one to authenticate the user, but it failed
     */
    const FAILURE_BREAK = false;

    /**
     * 100 - just go on. User is not authenticated but there's still no reason to stop
     */
    const FAILURE_CONTINUE = 100;
}
