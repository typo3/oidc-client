<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

/**
 * A list of constants related to this extension.
 */
class Constants
{
    /***********************************/
    /*      Extension constants        */
    /***********************************/

    /** The extension key */
    const EXT_KEY = 'oidc_client';

    /** The extension name: the key in PascalCase */
    const EXT_NAME = 'OidcClient';

    /** The extension prefix: the key without underscores */
    const PREFIX = 'oidcclient';

    /** The module name: the extension name prefixed with 'tx_' */
    const MOD_NAME = 'tx_oidcclient';

    /***********************************/
    /*        Icon identifiers         */
    /***********************************/

    const ICON_MODULE = 'module-oidc-client';

    const ICON_PROVIDER = 'oidc-icon';

    const ICON_USER = 'oidc-user';

    const ICON_GROUP = 'oidc-group';

    /***********************************/
    /*   Plugins & content elements    */
    /***********************************/

    const OIDC_BUTTONS = 'oidc_client_buttons';

    const PNAME_FEACCOUNT = 'FeAccount';

    const FEACCOUNT_PLUGIN = 'oidc_client_feaccount';

    /***********************************/
    /*       Tables & database         */
    /***********************************/

    /** The prefix for tables created by this extension */
    const TABLE_PREFIX = 'tx_oidcclient_';

    /** The table with configurations of providers */
    const TABLE_OIDC_CONFIG = 'tx_oidcclient_config';

    /** The table with oidc sessions */
    const TABLE_SESSION = "tx_oidcclient_session";

    /** The table with oidc data for frontend users */
    const TABLE_FEUSER = 'tx_oidcclient_feuser';

    /** The table with patterns to match frontend user groups with oidc roles */
    const TABLE_FEGROUP = 'tx_oidcclient_fegroup';

    /** The table with oidc data for backend users */
    const TABLE_BEUSER = 'tx_oidcclient_beuser';

    /** The table with patterns to match backend user groups with oidc roles */
    const TABLE_BEGROUP = 'tx_oidcclient_begroup';

    /** The extra field in fe_users/be_users for matching oidc identifiers */
    const TABLE_USER_OIDC = 'tx_oidcclient';

    /** The extra field in fe_groups/be_groups to match patterns */
    const TABLE_GROUP_PATTERN = 'tx_oidcclient_pattern';

    /***********************************/
    /*   Authentication feature flags  */
    /***********************************/

    // The meaning of bits in the fields `backend` and `frontend`
    // in tht table `tx_oidcclient_config`.
    const AUTH_ENABLED = 1;
    const AUTH_CREATE_USER = 2;
    const AUTH_UNLOCK_USER = 4;

    /***********************************/
    /*       Query parameter names     */
    /***********************************/

    /** The name of the query parameter with values for this extension */
    const REQUEST_PARAM = 'tx_oidcclient';

    /** The parameter indicating a new authentication request */
    const OIDC_PARAM_PROVIDER = 'tx_oidcclient_provider';

    /** The parameter indicating an on-going authentication request */
    const OIDC_PARAM_STATE = 'tx_oidcclient_state';

    const OAUTH_PARAM = 'oauth';

    /***********************************/
    /*    Session related constants    */
    /***********************************/

    /** The name of the cookie with an id of the oidc session */
    const DEFAULT_SESSION_COOKIE = 'oidc';

    /***********************************/
    /*    Routing related constants    */
    /***********************************/

    /** The base path for oidc route relative to the site base. This string must end with a slash. */
    const ROUTE_BASE = '/oidc/';

    /** The endpoint that redirects to a provider for authentication */
    const ROUTE_LOGIN = 'login';

    /** The default callback endpoint for providers */
    const ROUTE_CALLBACK = 'verify';

    /** Not an actual endpoint, but asks to process an authentication code  */
    const ROUTE_PROCESS = 'oidc/code';  // This never comes from a request uri

    /***********************************/
    /*    OIDC account types           */
    /***********************************/

    const FRONTEND_ACCOUNT = 'fe';

    const BACKEND_ACCOUNT = 'be';

    const ASPECT = 'oidc';
}

