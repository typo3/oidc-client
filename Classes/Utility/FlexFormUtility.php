<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class FlexFormUtility
{
    use StaticLoggerTrait;

    /**
     * Loads a flex form from a source string or a file. Returns null on errors.
     *
     * @param string $flexFormString
     * @return array|null
     */
    static public function loadFlexForm(string $flexFormString): ?array
    {
        if (str_starts_with($flexFormString, 'FILE:')) {
            $flexFormSource = @file_get_contents(
                GeneralUtility::getFileAbsFileName(substr($flexFormString,5))
            );
            if ($flexFormSource === false) {
                self::getLogger()->warning("Cannot locate $flexFormString");
                return null;
            }
            $flexFormString = $flexFormSource;
        }
        $parsed = GeneralUtility::xml2array($flexFormString);
        if (!is_array($parsed)) {
            self::getLogger()->error("Invalid flex form: $parsed");
            return null;
        }
        return $parsed;
    }

    /**
     * Extract settings from a flex data, grouped by sheets.
     *
     * @param string|null $flexData
     * @return array
     */
    static public function flexData2array(?string $flexData): array
    {
        if (!$flexData) return [];
        $result = [];
        $xml = GeneralUtility::xml2array($flexData)['data'];
        foreach ($xml as $sheet => $data) {
            // Ignore sheets with no key
            if (!is_string($sheet)) continue;

            $values = [];
            foreach ($data['lDEF'] as $setting => $content) {
                if (str_starts_with($setting, 'settings.')) {
                    $values[substr($setting, 9)] = $content['vDEF'] ?? '';
                }
            }
            // skip initial 's_'
            $result[substr($sheet,2)] = $values;
        }
        return $result;
    }

}