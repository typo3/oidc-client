<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

use Traversable;
use Countable;
use IteratorAggregate;
use RuntimeException;

/**
 * A collection of key-value pairs with ordered keys that allows
 * to insert new elements with respect to two sets of keys: those
 * that should be before and those that should be after the inserted
 * one.
 */
class OrderedMap implements IteratorAggregate, Countable
{
    /**
     * @var array[] $before  A mapping (element) => (elements that must be before)
     *
     * It is not required that all the keys in the lists are present in this collection.
     * However, every key present in these lists must be also a key of this array, i.e.
     * `array_keys($before)` contains `array_keys($before[$key])` for every `$key`.
     */
    private array $before = [];

    /** @var mixed[] $values  A collection of values */
    private array $values = [];

    /**
     * Inserts an element to the collection
     *
     * @param string $element   the key of the value
     * @param mixed $value      the inserted value
     * @param array $after      keys of elements after which this element must be inserted
     * @param array $before     keys of elements before which this element must be inserted
     * @return void
     */
    public function add(string $element, mixed $value, array $after, array $before): void
    {
        $this->values[$element] = $value;
        // Update keys of elements that must be before this one
        $this->before[$element] = array_unique(array_merge($this->before[$element] ?? [], $after));
        // Add the identifier to the list of preceding keys for each entry in $before
        foreach ($before as $identifier) {
            if (!isset($this->before[$identifier])) {
                $this->before[$identifier] = [$element];
            } elseif (!in_array($element, $this->before[$identifier])) {
                $this->before[$identifier][] = $element;
            }
        }
        // Make sure that each referred id has an entry in $this->before.
        foreach ($after as $identifier) {
            if (!isset($this->before[$identifier])) $this->before[$identifier] = [];
        }
    }

    public function has(string $element): bool
    {
        return isset($this->values[$element]);
    }

    public function count(): int
    {
        return count($this->values);
    }

    /**
     * Iterates over items from this collection respecting the order
     * requested when the items where inserted. Throws when a cyclic
     * condition is detected.
     *
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        $elements = array_keys($this->before);
        $count = count($elements);
        $ordered = [];
        $firstUnprocessed = 0;
        while ($firstUnprocessed < $count) {
            $foundNext = false;
            for ($i = $firstUnprocessed; $i < $count; $i++) {
                $id = $elements[$i];
                if (count(array_intersect($this->before[$id], $ordered)) === count($this->before[$id])) {
                    $foundNext = true;
                    $ordered[] = $id;
                    $elements[$i] = $elements[$firstUnprocessed];
                    $firstUnprocessed++;
                    if (isset($this->values[$id])) {
                        yield $id => $this->values[$id];
                    }
                }
            }
            if (!$foundNext) {
                throw new RuntimeException("a cyclic dependence of processors: " . implode(', ', array_splice($elements, $firstUnprocessed)), 1709585794);
            }
        }
    }

    public function toArray(): array
    {
        return iterator_to_array($this->getIterator());
    }
}
