<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * A helper class to manage resource processors.
 */
class PluginManager
{
    use StaticLoggerTrait;

    /**
     * @var OrderedMap $processors
     * A list of processor from last to first. Thanks to this convention,
     * processors added with no restrictions will be placed at the end
     * of the list.
     */
    private static OrderedMap $processors;


    static private function createFlexFormMessage(string $processorId, string $message): array
    {
        self::getLogger()->error("configuration error for $processorId: $message");
        return ['ROOT' => [
            'sheetTitle' => $processorId . ' [error]',
            'el' => [
                'error-' . $processorId => [
                    'label' => 'Configuration form error',
                    'type' => 'none',
                    'renderType' => 'text',
                    'readOnly' => true,
                    'default' => $message
                ]
            ]
        ]];
    }

    private static function asArray(mixed $value): array
    {
        return empty($value) ? [] : (is_array($value) ? $value : [$value]);
    }

    static public function addResourceProcessor(
        string $processorId,
        string $flexFormString,
        array $options = []
    ): void {

        if (!isset(self::$processors)) {
            self::$processors = new OrderedMap();
        } elseif (self::$processors->has($processorId)) {
            // Cannot override an existing processor
            self::getLogger()->error("$processorId already exists");
            throw new \RuntimeException("Configuration error: a duplicated processor '$processorId' for the OpenID Connect client");
        }
        
        // For now cache save only the configuration in the ordered map.
        // The column pi_flexform will be generated later.
        self::$processors->add(
            $processorId,
            $flexFormString,
            // Recall that $processors stores the configurations
            // in a reversed order
            self::asArray($options['before'] ?? null),
            self::asArray($options['after'] ?? null)
        );
    }

    static private function validateProcessorConfiguration(string $processorId, string $configuration): array
    {
        $flexForm = FlexFormUtility::loadFlexForm($configuration);
        if (isset($flexForm['sheets'])) {
            if (count($flexForm['sheets']) === 1) {
                // Get the sheet without knowing its key
                $flexForm = reset($flexForm['sheets']);
            } else {
                return self::createFlexFormMessage($processorId, 'The configuration form cannot contain sheets');
            }
        }
        if (!ArrayUtility::isValidPath($flexForm, 'ROOT/el')) {
            return self::createFlexFormMessage($processorId, 'no element ROOT > el');
        }
        // Add the 'enabled' field unless already specified
        if (!isset($flexForm['ROOT']['el']['settings.enabled'])) {
            $flexForm['ROOT']['el'] = array_merge([
                'settings.enabled' => [
                    'label' => 'Enable',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [ [0 => '', 1 => ''] ]
                    ]
                ]
            ], $flexForm['ROOT']['el']);
        }
        return $flexForm;
    }

    static public function generateProcessingConfigurationForm(): array
    {
        if (!isset(self::$processors)) {
            return [];
        }

        $sheets = [];
        foreach (self::$processors as $processorId => $configuration) {
            if ($configuration) {
                $flexForm = FlexFormUtility::loadFlexForm($configuration);
                if (!$flexForm) {
                    $sheets["s_$processorId"] = self::createFlexFormMessage($processorId, "failed to parse configuration: invalid XML document");
                } else {
                    $sheets["s_$processorId"] = self::validateProcessorConfiguration($processorId, $configuration);
                }
            }
        }
        return ['sheets' => $sheets];
    }

}
