<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\Utility;

use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A logger for static classes
 */
trait StaticLoggerTrait
{
    static protected ?Logger $logger = null;

    /**
     * Returns a logger object for this class. The logger is created
     * on the first access; subsequent calls ignore the parameters.
     *
     * @param string|null $name     default: the class FQ name
     * @return Logger
     */
    static protected function getLogger(?string $name = null): Logger
    {
        if (!self::$logger) {
            self::$logger = GeneralUtility::makeInstance(LogManager::class)->getLogger($name ?? static::class);
        }
        return self::$logger;
    }
}
