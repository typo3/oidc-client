<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\ViewHelpers;

use IMATHUZH\OidcClient\Domain\Model\ProviderLabel;
use IMATHUZH\OidcClient\Utility\Constants;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class ButtonViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;
    protected $escapeChildren = false;

    /**
     * Initializes the arguments
     * @return void
     */
    public function initializeArguments(): void
    {
        $this->registerArgument(
            'provider',
            ProviderLabel::class,
            'The OpenID Connect provider',
            true);
        $this->registerArgument('class', 'string', 'The CSS class of the button', false, 'btn btn-default');
    }

    /**
     * Prints a button with the icon and title of an OpenID Connect provider
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext): string
    {
        /** @var ProviderLabel $provider */
        $provider = $arguments['provider'];
        $class = $arguments['class'];

        $icon = ProviderIconRenderer::render($provider, $renderingContext);

        return '<button type="submit" name="' . Constants::OIDC_PARAM_PROVIDER
            . '" value="' . $provider->getUid()
            . '" class="' . htmlspecialchars($class)
            . '"><span class="oidc-icon">' . $icon . '</span>' . htmlspecialchars($provider->getTitle()) . '</button>';
    }

}
