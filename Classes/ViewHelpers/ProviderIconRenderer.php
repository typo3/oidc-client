<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace IMATHUZH\OidcClient\ViewHelpers;

use IMATHUZH\OidcClient\Domain\Model\ProviderLabel;
use TYPO3\CMS\Core\ViewHelpers\IconViewHelper;
use TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * A helper class to render a provider logo
 *
 * @todo Expand to an actual view helper
 */
class ProviderIconRenderer
{
    public static function render(ProviderLabel $provider, RenderingContextInterface $renderingContext): string
    {
        return self::asImage($provider, $renderingContext)
            ?: self::asIcon($renderingContext);
    }

    public static function asImage(ProviderLabel $provider, RenderingContextInterface $renderingContext): ?string
    {
        // IconViewHelper throws an exception when the image file is not processed yet
        // and the current user cannot read folders. This happens, for instance, when
        // displaying the oidc login provider form in the backend login page.
        try {
            if (($logo = $provider->getLogo())) {
                return ImageViewHelper::renderStatic([
                    'image' => $logo,
                    'height' => '20',
                    'title' => $provider->getTitle()
                ], fn() => '', $renderingContext);
            }
        } catch (\Exception $e) {
            // failure: return null
        }
        return null;
    }

    public static function asIcon(RenderingContextInterface $renderingContext): string
    {
        return IconViewHelper::renderStatic([
            'identifier' => 'oidc-icon',
            'size' => 'small',
            'overlay' => null,
            'state' => null,
            'alternativeMarkupIdentifier' => null,
        ], fn() => '', $renderingContext);
    }}