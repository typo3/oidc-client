<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

return [
    \IMATHUZH\OidcClient\Domain\Model\ProviderLabel::class => [
        'tableName' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_OIDC_CONFIG
    ],

    \IMATHUZH\OidcClient\Domain\Model\ProviderCard::class => [
        'tableName' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_OIDC_CONFIG
    ]
];
