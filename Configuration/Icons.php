<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

defined('TYPO3') or die();

return (static function() {

    $iconFolder = 'EXT:'.\IMATHUZH\OidcClient\Utility\Constants::EXT_KEY.'/Resources/Public/Icons';
    return [
        \IMATHUZH\OidcClient\Utility\Constants::ICON_MODULE => [
            'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            'source' => "$iconFolder/Extension.svg"
        ],
        \IMATHUZH\OidcClient\Utility\Constants::ICON_PROVIDER => [
            'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            'source' => "$iconFolder/Icon.svg"
        ],
        \IMATHUZH\OidcClient\Utility\Constants::ICON_USER => [
            'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            'source' => "$iconFolder/User.svg"
        ],
        \IMATHUZH\OidcClient\Utility\Constants::ICON_GROUP => [
            'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            'source' => "$iconFolder/Group.svg"
        ],
    ];

})();
