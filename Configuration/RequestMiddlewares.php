<?php

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

defined('TYPO3') or die();

return [
    'frontend' => [
        'imathuzh/oidc-client' => [
            'target' => \IMATHUZH\OidcClient\Middleware\OidcRequestHandler::class,
            'after' => [ ],
            'before' => [
                'typo3/cms-frontend/authentication',
                'typo3/cms-frontend/backend-user-authentication',
                'typo3/cms-frontend/page-resolver'
            ]
        ]
    ],
    'backend' => [
        'imathuzh/oidc-client' => [
            'target' => \IMATHUZH\OidcClient\Middleware\OidcRequestHandler::class,
            'after' => [ ],
            'before' => [
                'typo3/cms-backend/authentication',
                'typo3/cms-backend/site-resolver'
            ]
        ]
    ]
];
