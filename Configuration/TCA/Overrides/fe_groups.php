<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

(static function () {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $patternField = \IMATHUZH\OidcClient\Utility\Constants::TABLE_GROUP_PATTERN;
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf";

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', [
        $patternField => [
            'exclude' => true,
            'label' => "$lll:groups.pattern",
            'description' => "$lll:groups.pattern.description",
            'config' => [
                'type' => 'inline',
                'appearance' => [
                    'collapseAll' => false,
                    'showNewRecordLink' => true,
                    'newRecordLinkTitle' => 'Create a new pattern',
                    'levelLinksPosition' => 'bottom',
                    'enableControls' => [
                        'info' => true,
                        'hide' => true,
                        'delete' => true
                    ],
                ],
                'foreign_table' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_FEGROUP,
                'foreign_field' => 'group',
                'enableCascadingDelete' => true,
            ]
        ],
    ]);

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'fe_groups',
        "--div--;$lll:tab.patterns,$patternField",
        '',
        'after:description'
    );

})();
