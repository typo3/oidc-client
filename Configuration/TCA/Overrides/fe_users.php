<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

(static function () {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $oidcField = \IMATHUZH\OidcClient\Utility\Constants::TABLE_USER_OIDC;
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf";

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', [
        $oidcField => [
            'exclude' => true,
            'label' => "$lll:users.oidc",
            'description' => "$lll:users.oidc.description",
            'config' => [
                'type' => 'inline',
                'appearance' => [
                    'collapseAll' => false,
                    'showNewRecordLink' => false,
                    'enableControls' => [
                        'info' => true,
                        'hide' => true,
                        'delete' => true
                    ]
                ],
                'foreign_table' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_FEUSER,
                'foreign_field' => 'user',
                'enableCascadingDelete' => true,
            ]
        ],
    ]);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'fe_users',
        "--div--;$lll:tab.authentication,$oidcField",
        '',
        'after:description'
    );

})();
