<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * TypoScript configuration for OIDC redirect buttons
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY,
    'Configuration/TypoScript',
    'Rendering configuration for OIDC buttons'
);
