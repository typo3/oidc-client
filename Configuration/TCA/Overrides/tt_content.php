<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

(static function () {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $oidcIcon = \IMATHUZH\OidcClient\Utility\Constants::ICON_PROVIDER;

    /**** Content element: OIDC buttons ****/

    $oidcButtons = \IMATHUZH\OidcClient\Utility\Constants::OIDC_BUTTONS;
    // Register the content element with OIDC buttons
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            "LLL:EXT:$extkey/Resources/Private/Language/locallang.xlf:oidc_buttons.label",
            $oidcButtons,
            $oidcIcon
        ]
    );
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$oidcButtons] = $oidcIcon;

    // Configure the default backend fields for the content element
    $GLOBALS['TCA']['tt_content']['types'][$oidcButtons] = [
        'showitem' => '
             --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;;general,
                header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header.ALT.div_formlabel,
                pi_flexform,
             --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;;frames,
                --palette--;;appearanceLinks,
             --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
             --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;;access,
             --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
             --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription
          ',
        'columnsOverrides' => [
            'header' => [
                'description' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header.description.ALT',
            ]
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        "FILE:EXT:$extkey/Configuration/FlexForms/OidcButtons.xml",
        $oidcButtons
    );

    /**** Content element: frontend account management ****/

    $feAccountMgr = \IMATHUZH\OidcClient\Utility\Constants::PNAME_FEACCOUNT;
    $piSign = 'oidcclient_feaccount';

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $extkey,
        $feAccountMgr,
        'Listing of linked OIDC accounts',
        'oidc-icon',
        'forms'
    );
//    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$piSign] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        "FILE:EXT:$extkey/Configuration/FlexForms/OidcButtons.xml",
        $piSign
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.plugin, pi_flexform',
        $piSign,
        'after:palette:headers'
    );


})();
