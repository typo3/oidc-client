<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

return (static function() {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $modname = \IMATHUZH\OidcClient\Utility\Constants::MOD_NAME;
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf:{$modname}_group";
    $lllbe = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf:{$modname}_begroup";

    return [
        'ctrl' => [
            'title' => $lllbe,
            'label' => 'pattern',
            'label_userFunc' => \IMATHUZH\OidcClient\Backend\TCA::class . '->oidcBeGroupRecordTitle',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'delete' => 'deleted',
            'searchFields' => 'sub',
            'enablecolumns' => [
                'disabled' => 'disable'
            ],
            'rootLevel' => 1,
//            'hideTable' => true,
            'iconfile' => "EXT:$extkey/Resources/Public/Icons/Group.svg", // 'oidc-icon'
        ],
        'columns' => [
            'disable' => [
                'exclude' => 1,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [0 => '', 1 => '']
                    ]
                ],
            ],
            'group' => [
                'exclude' => 1,
                'label' => "$lll.group",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'be_groups',
                    'required' => true,
                    'default' => 0
                ]
            ],
            'claim' => [
                'exclude' => 1,
                'label' => "$lll.claim",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'maxSize' => 40,
                    'required' => true
                ]
            ],
            'pattern' => [
                'exclude' => 1,
                'label' => "$lll.pattern",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'maxsize' => 255,
                    'required' => true
                ]
            ],
            'provider' => [
                'label' => "$lll.provider",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_OIDC_CONFIG,
                    'required' => true
                ]
            ]
        ],
        'palettes' => [
            'data' => [
                'showitem' => 'group,provider,claim,pattern'
            ]
        ],
        'types' => [
            [
                'showitem' => '--palette--;;data',
            ]
        ],
    ];
})();
