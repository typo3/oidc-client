<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

return (static function() {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf";
    $llldb = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf:config";
    $llltabs = "LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf";

    return [
        'ctrl' => [
            'title' => $llldb,
            'label' => 'title',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'delete' => 'deleted',
            'searchFields' => 'title,name',
            'sortby' => 'sorting',
            'enablecolumns' => [],
            'rootLevel' => 1,
            'selicon_field' => 'logo',
            'iconfile' => "EXT:$extkey/Resources/Public/Icons/Icon.svg", // 'oidc-icon',
//            'typeicon_column' => 'name',
//            'typeicon_classes' => [
//                'default' => 'oidc-icon'
//            ]
        ],
        'columns' => [
            'sorting' => [
                'exclude' => 1,
                'label' => 'Sorting',
                'config' => [
                    'type' => 'passthrough'
                ]
            ],
            'name' => [
                'exclude' => 1,
                'label' => "$llldb.name",
                'description' => "$llldb.name.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 31,
                    'eval' => 'trim,alphanum_x',
                    'required' => true
                ]
            ],
            'title' => [
                'label' => "$llldb.title",
                'description' => "$llldb.title.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 63,
                    'eval' => 'trim',
                    'required' => true
                ]
            ],
            'logo' => [
                'label' => "$llldb.logo",
                'description' => "$llldb.logo.description",
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'logo',
                    [
                        'maxitems' => 1,
                        'appearance' => [
                            'collapseAll' => true,
                            'useSortable' => false,
                            'enabledControls' => [ 'hide' => false ]
                        ]
                    ]
                )
            ],
            'description' => [
                'label' => "$llldb.notes",
                'description' => "$llldb.notes.description",
                'config' => [
                    'type' => 'text',
                    'cols' => 20,
                    'rows' => 3
                ]
            ],
            'auth_endpoint' => [
                'label' => "$llldb.endpoint.auth",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 255,
                    'required' => true
                ]
            ],
            'token_endpoint' => [
                'label' => "$llldb.endpoint.token",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 255,
                    'required' => true
                ]
            ],
            'userinfo_endpoint' => [
                'label' => "$llldb.endpoint.userinfo",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 255,
                    'required' => false
                ]
            ],
            'logout_endpoint' => [
                'label' => "$llldb.endpoint.logout",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 255,
                    'required' => false
                ]
            ],
            'revoke_endpoint' => [
                'label' => "$llldb.endpoint.revoke",
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                    'size' => 50,
                    'max' => 255,
                    'required' => false
                ]
            ],
            'callback_forwarding' => [
                'exclude' => 1,
                'label' => "$llldb.callback",
                'description' => "$llldb.callback.description",
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [0 => '', 1 => '']
                    ]
                ],
            ],
            'lang_parameter' => [
                'exclude' => 1,
                'label' => "$llldb.lang-param",
                'description' => "$llldb.lang-param.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 63,
                    'required' => false,
                    'default' => 'language'
                ]
            ],
            'client_id' => [
                'label' => "$llldb.client.id",
                'description' => "$llldb.client.id.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 127,
                    'required' => true
                ]
            ],
            'client_secret' => [
                'label' => "$llldb.client.secret",
                'description' => "$llldb.client.secret.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 127,
                    'required' => true
                ]
            ],
            'client_scopes' => [
                'label' => "$llldb.client.scopes",
                'description' => "$llldb.client.scopes.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 127,
                    'required' => false
                ]
            ],
            'mfa' => [
                'label' => "$llldb.auth.mfa",
                'description' => "$llldb.auth.mfa.description",
                'config' => [
                    'type' => 'check',
                    'items' => [
                        [0 => "$llldb.auth.mfa.label", 1 => '']
                    ]
                ]
            ],
            'backend' => [
                'exclude' => 1,
                'label' => "$llldb.backend",
                'config' => [
                    'type' => 'check',
                    'items' => [
                        [0 => "$llldb.auth.enable", 1 => ''],
                        [0 => "$llldb.auth.create-user", 1 => ''],
                        [0 => "$llldb.auth.unlock-user", 1 => ''],
                    ],
                    'default' => 1
                ]
            ],
            'frontend' => [
                'exclude' => 1,
                'label' => "$llldb.frontend",
                'config' => [
                    'type' => 'check',
                    'items' => [
                        [0 => "$llldb.auth.enable", 1 => ''],
                        [0 => "$llldb.auth.create-user", 1 => ''],
                        [0 => "$llldb.auth.unlock-user", 1 => ''],
                    ],
                    'default' => 1
                ]
            ],
            'be_groups' => [
                'exclude' => 1,
                'label' => "$llldb.backend.groups",
//                'description' => "$llldb.groups.description",
                'config' => [
                    'type' => 'group',
                    'allowed' => 'be_groups',
                    'foreign_table' => 'be_groups',
                    'minitems' => 0,
                    'maxitmes' => 30,
                    'fieldControl' => [
                        'editPopup' => ['disabled' => false],
                        'addRecord' => ['disabled' => false]
                    ]
                ]
            ],
            'fe_groups' => [
                'exclude' => 1,
                'label' => "$llldb.frontend.groups",
//                'description' => "$llldb.groups.description",
                'config' => [
                    'type' => 'group',
                    'allowed' => 'fe_groups',
                    'foreign_table' => 'fe_groups',
                    'minitems' => 0,
                    'maxitmes' => 30,
                    'fieldControl' => [
                        'editPopup' => ['disabled' => false],
                        'addRecord' => ['disabled' => false]
                    ]
                ]
            ],
            'processors' => [
                'label' => "$llldb.processors",
                'description' => "$llldb.processors.description",
                'config' => [
                    'type' => 'none',
                ]
            ],
            'mapping' => [
                'label' => "$llldb.mapping",
                'description' => "$llldb.mapping.description",
                'config' => [
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 40,
                    'wrap' => 'off',
                    'required' => false,
                    'eval' => \IMATHUZH\OidcClient\Backend\ClaimMappingEval::class,
                    'default' => null
                ]
            ],
            'id_claim' => [
                'label' => "$llldb.id-claim",
                'description' => "$llldb.id-claim.description",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'max' => 31,
                    'eval' => 'trim',
                    'default' => 'sub',
                    'required' => true
                ]
            ],
        ],
        'palettes' => [
            'appearance' => [
                'label' => "$llldb.appearance",
                'description' => "$llldb.appearance.description",
                'showitem' => 'title, --linebreak--, logo'
            ],
            'client' => [
                'label' => "$llldb.client",
                'description' => "$llldb.client.description",
                'showitem' => '
                    client_id, --linebreak--,
                    client_secret, --linebreak--,
                    client_scopes'
            ],
            'endpoints' => [
                'label' => "$llldb.endpoint",
                'description' => "$llldb.endpoint.description",
                'showitem' => '
                    auth_endpoint, --linebreak--,
                    token_endpoint, --linebreak--,
                    revoke_endpoint, --linebreak--,
                    logout_endpoint, --linebreak--,
                    userinfo_endpoint'
            ],
            'authentication' => [
                'label' => "$llldb.auth",
                'description' => "$llldb.auth.description",
                'showitem' => 'backend, frontend'
            ],
            'user-groups' => [
                'label' => "$llldb.groups",
                'description' => "$llldb.groups.description",
                'showitem' => 'be_groups, fe_groups'
            ]
        ],
        'types' => [
            [
                'showitem' => "
                    --div--;$llltabs:general,
                        name,
                        --palette--;;appearance,
                    --div--;$lll:tab.authentication,
                        mfa,
                        --palette--;;authentication,
                        --palette--;;user-groups,
                    --div--;$lll:tab.client,
                        --palette--;;client,
                    --div--;$lll:tab.provider,
                        callback_forwarding,
                        --palette--;;endpoints,
                    --div--;$lll:tab.mapping-rules,
                        id_claim, mapping,
                    --div--;$llltabs:notes,
                        description",
            ]
        ],
    ];
})();
