<?php
defined('TYPO3') or die();

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

return (static function() {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $modname = \IMATHUZH\OidcClient\Utility\Constants::MOD_NAME;
    $lllfe = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf:{$modname}_feuser";
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang_db.xlf:{$modname}_user";

    return [
        'ctrl' => [
            'title' => $lllfe,
            'label' => 'sub',
            'label_userFunc' => \IMATHUZH\OidcClient\Backend\TCA::class . '->oidcFeUserRecordTitle',
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'delete' => 'deleted',
            'searchFields' => 'sub',
            'enablecolumns' => [
                'disabled' => 'disable'
            ],
//            'hideTable' => true,
            'iconfile' => "EXT:$extkey/Resources/Public/Icons/User.svg", // 'oidc-user'
        ],
        'columns' => [
            'disable' => [
                'exclude' => 1,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [0 => '', 1 => '']
                    ]
                ],
            ],
            'sub' => [
                'exclude' => 1,
                'label' => "$lll.sub",
                'config' => [
                    'type' => 'input',
                    'size' => 40,
                    'readOnly' => true
                ]
            ],
            'provider' => [
                'label' => "$lll.provider",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => \IMATHUZH\OidcClient\Utility\Constants::TABLE_OIDC_CONFIG,
                    'readOnly' => true
                ]
            ],
            'user' => [
                'label' => "$lll.user",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'fe_users'
                ]
            ]
        ],
        'palettes' => [
            'data' => [
                'showitem' => 'user,provider,sub'
            ]
        ],
        'types' => [
            [
                'showitem' => '--palette--;;data',
            ]
        ],
    ];
})();
