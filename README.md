# OIDC Authentication for Typo3

This extenion allows users to authenticate with several OpenID Connect providers both in the frontend and backend. Each Typo3 account can be associated with several oidc accounts and the extension provides a frontend content element for managing these accounts by the user (currently only for frontend).

During the authentication process the extension attempts to identify the correct local Typo3 account. When none is found or the matching account is disabled, the extension can be configured either to present an error message to the user or to create/enable the account.


# Basic installation and usage

**Step 1**: Typo3 backend
* Install `oidc_client` from TER or composer
* Setup extension configuration in *Settings &rarr; oidc_client*
* Create providers via the OIDC module

**Step 2**: Typo3 frontend
* *Login Form* element: use the tab *Plugin &rarr; OpenID Connect* to select providers and to tweak the behavior of authentication buttons
* *OpenID Connect accounts* element: user the tab *Plugin* to select providers and to tweak the behavior of authentication buttons

Consult the following sections for details on the above steps.

# Extension settings

In most cases the default values are fine. If a setting is not described below and you do not understand its description on the configuration page, then it is better to leave it as it is :-)

## Generic settings

### Client scopes

The default space-separated list of OIDC scopes requested from a provider. It should contain at least *openid* with other common items being *email* and *profile*. This settings is overriden by the corresponding property of an identity provider.

### Extension endpoint

An absolute path that ends with `/`; all requests to this path are intercepted and processed by this provider. Example: `/my-app/oidc/` for a website accessible under `http://domain.com/my-app`.

### Callback URI

A space-separated list of urls that can be used as callbacks. Each item can be a full URL or an absolute path and the callback URL passed to an identity provider is chosen as follows:
 * if the current domain matches any full URL, then the first matching URL is chosen
 * if no full URL matches and an absolute path is listed, then the first path is chosen with the current domain prepended
 * otherwise the default location is returned: `http(s)://<current host>/<extension endpoint>/verify`

The resulting URL must be registered and accepted by the identity provider.

### Cookie name

The name of the cookie that stores the oidc session id

### Cookie path

The path under which the cookie is stored; best the root path of the application

### Security token key

A 32 character token used to secure oidc parameters passed between requests; use  a secure random token generator such as KeePass to generate a good token.

## Advanced

### Callback forwarding service

If this instance is not registered with an identity provider then this feature allows to use another website (registered with the identity provider) to be used for oidc requests instead. This is a beta-feature that currently requires a separate code on the service page. Please contact support@math.uzh.ch for details if you need this feature.

### Revoke TYPO3's access token at the end of the login process

Determines whether the access token will be revoked after the authentication. This is usually not necessary as the tokens are usually short-lived and cannot be reused.

### Enable PKCE

An additional security feature of oidc protocol; enable when supported by the provider.

### Disable CSRF attack mitigation

Disables certain checks during processing the authentication code returned by an identity provider. While it could be useful in tests, this feature **should be always disabled in production**.

### TYPO3 authentication service properties

Consult [Typo3 documention on services](https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Services/UsingServices/ServicePrecedence.html).


## Frontend / Backend

### Redirect URI

The URL to which the extension redirects the user once the authentication code returned by an identity provider is processed; *can be empty*. The final URL is the first non-empty value from the following list determined from the initial authentication request:
 * `redirect_url` request parameters
 * `referer` request parameters
 * *Redirect URI* setting
 * request target URL (frontend) or the path to `/typo3/main` (backend)

Once computed, the final URL is added the `logintype` (frontend) or `login_status` (backend) query parameters if such a parameter is present in the authentication request.

### Backend / frontend authentication

Enables the oidc authentication service. This is the master switch - when switched off then oidc authentications will not be processed at all.

### Recycle users

If a local typo3 account must be created for an authentication user and there is already a deleted record, then this record will be reused and updated instead of creating a new one.


# Creating and editing providers

There are two ways to create / edit an identity provider:
1. by adding a record to the root page
1. from the OIDC module page

The OIDC module page lists all configured providers and allows to configure quickly
* authentication features (see below)
* the order of providers

The configuration page of a provider consists of six tabs.
1. *General*: enter a unique identifier string, the name shown on a button, and optionally choose a logo image (best is to save all images in a dedicated folder, e.g. `fileadmin/oidc`)
1. *Authentication*: select authentication features, default user groups (assigned to accounts created by thsi extension), and behavior with respect to Typo3 MFA
1. *Client*: enter client id and key as registered on the provider. When a callback forwarding service is used, then these are the credentials for the service website. Client scopes can be left emtpy - in such case the scopes from the exension settings are used.
1. *Provider*: various endpoints of the provider. Check the provider's page how to find them
1. *Mapping rules*: define the claim with the owner id (usually `sub`) and how to generate Typo3 user properties from the resource. Seperate several options for a property with `//` - in such case the first non-trivial value is used. Lines starting with `#` are ignored.
   <pre>
   # For username: use email if no shortname exists.
   # Note: if the final result is empty, then resource ID is taken as the username!!!
   username = &lt;shortname&gt; // &lt;email&gt;
   name = &lt;family_name&gt;, &lt;given_name&gt;
   first_name = &lt;given_name&gt;
   last_name = &lt;family_name&gt;
   email = &lt;email&gt;
   </pre>
   If resource processors are added (such as `oidc-resource-ldap-proc`), an additional processing of the resource can be configured below. Resource processors are called *before* the above mapping is done.
1. *Notes*: additional notes, not used by the extension.


# Usage in frontend

## Supported content elements

### Login form (`fe_login`)

Configuration tab: *Plugin &rarr; OpenID Connect*.

Additional buttons can be shown on the login page for selected providers.
In order to include those buttons the following changes should be made to the login template
`fe_login:Resources/Private/Template/Login.html`:
* OIDC errors are reported via the variable `oidcError`. When nonempty, then `messageKey` has the value `error`
* Enabled providers are given in a table `oidcProviders`. A button for each provider can be rendered with the `oidc:button` view helper.

Consult the example from `oidc_client:Resources/Private/Templates/Felogin`.

#### Example
<pre>

&lt;f:if condition="{messageKey}"&gt;
    &lt;h3&gt;
        &lt;f:render partial="RenderLabelOrMessage" arguments="{key: '{messageKey}_header'}"/&gt;
    &lt;/h3&gt;
    &lt;p&gt;&lt;f:if condition="{oidcError}"&gt;
        &lt;f:then&gt;{oidcError}&lt;/f:then&gt;
        &lt;f:else&gt;&lt;f:render partial="RenderLabelOrMessage" arguments="{key: '{messageKey}_message'}"/&gt;&lt;/f:else&gt;
    &lt;/f:if&gt;&lt;/p&gt;
&lt;/f:if&gt;

...

&lt;f:if condition="{providers}"&gt;
    &lt;f:asset.css identifier="oidc-links" href="EXT:oidc_client/Resources/Public/Css/links-felogin.css"/&gt;
    &lt;form class="oidc-links" name="oidc-links" action="" method="POST"&gt;
        &lt;f:for each="{oidcProviders}" as="provider"&gt;
            &lt;oidc:button provider="{provider}" class="btn btn-block btn-default"/&gt;
        &lt;/f:for&gt;
        &lt;input type="hidden" name="logintype" value="login"/&gt;
        &lt;input type="hidden" name="tx_oidcclient" value="{oidcSettings}"/&gt;
        &lt;f:if condition="{settings.promptOidcLogin}"&gt;
            &lt;input type="hidden" name="oauth[prompt]" value="login" /&gt;
        &lt;/f:if&gt;
    &lt;/form&gt;
&lt;/f:if&gt;

</pre>


### OpenID connect accounts (`oidc_client`)

Configuration tab: *Plugin*

This content elements renders a table with oidc accounts associated with the logged-in user. Each account can be updated or deleted (unlinked). The latter is possible also for disabled account, but in this case the data is not completely erased from the database, so that the user cannot authenticated again with this account after deleting it (unless allowed to do so in the configuration of this content element).

A list of authentication button for selected providers is listed below the table, allowing the user to link easily another oidc accounts with them. This feature allows to add such accounts in case the user cannot be found during the normal login process.


## Configuring OIDC features in content elements

### Select providers

Only selected providers will be shown. Providers that are not enabled in frontend are marked with the string **[disabled]** next to their names.

### Re-enable accounts

This feature allow the extension to unlock a disabled account on a successful authentication. Otherwise the user is rejected with an error message.

### Always ask for user credentials

If a user has been already authentication with an identity provider then the provider does not ask for the credentials during authentication. This feature requests the identity provider to ask for credentials anyway. This could be used in the following situations:
 * a user accesses a more restricted section of the page, in which case asking for the credentials is desired to secure the access
 * a user would like to authenticate themself with a different account from the same provider.


# A overview of (some) design concepts

## Matching a local account with an OIDC resource

A local typo3 account `A` is matched with a resource `R` as follows:
1. If the oidc account is already linked to an (undeleted) account, then this account is chosen.
1. Otherwise the extension searches for an undeleted user account with `A.username` matching `R.shortname`
   or `A.email` matching `R.email` (if the first match fails).
1. Such an account is chosen only if one is found.

If no account (or more than one) matching is found, then a new account is created unless configured otherwise in the authentication features of the provider.


## Resource processors

An OIDC resource can be processed by additional resource processors. See the extension `oidc-resource-ldap-proc` for an example.

### Implementing a processor

A resource processor is a listener to `ModifyResourceOwnerEvent` and as such it must implement the magic method `__invoke(ModifyResourceOwnerEvent $event)`. The event parameter can be used to read and update resource claims:
<pre>
$resource = $event-&gt;getResourceOwner()  // this is a reference
$email = $resource-&gt;get('email');
$resource-&gt;set('shortname', $username);
</pre>
as well as to tweak authentication features for this requests:
<pre>
/** @var OidcAspect $aspect */
$aspect = $event-&gt;getAspect();   // this is a reference

// check if the current provider is enabled
$isEnabled = (bool)$aspect->getAuthFeatures(Constants::AUTH_ENABLED);

// allow to create a local account independently of the settings
$aspect->toggleAuthFeatures(Constants::AUTH_CREATE_USER, true);

// set all features at once:
//    ENABLED and CREATE_USER to true
//    UNLOCK_USER to false
$aspect->setAuthFeatures(Constants::AUTH_ENABLED|Contants::AUTH_CREATE_USER);
</pre>

### Registering a processor

Register a resource processor in `your_ext:Configuration/TCA/Overrides/tx_oidcclient_config.php` with the method
<pre>
\IMATHUZH\OidcClient\Utility\PluginManager::addResourceProcessor(
    string $processorId,
    string $flexFormConfiguration
);
</pre>
and declare it as `event.listener` in `your_ext:Configuration/Services.yaml`:
<pre>
  FullQualifiedNameOfYourProcessorClass:
    tags:
      - name: event.listener
        identifier: &lt;processorId&gt;
</pre>
The provided Flex form is displayed on the *Mapping rules* tab of identity provider's configuration page and the values can be obtained from the event objects via
<pre>
$event-&gt;getProcessorConfig($processorId);
</pre>
