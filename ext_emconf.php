<?php

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'OpenID Connect authentication',
    'description' => 'This extension allows to authenticate a user with one of predefined OpenID Connect providers.',
    'category' => 'services',
    'version' => '0.3.2',
    'state' => 'beta',
    'author' => 'Krzysztof Putyra',
    'author_email' => 'support@math.uzh.ch',
    'author_company' => 'I-MATH UZH',
    'constraints' => [
        'depends' => [
            'php' => '8.0.0-8.2.99',
            'typo3' => '11.5.0-12.4.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'IMATHUZH\\OidcClient\\' => 'Classes'
        ]
    ]
];
