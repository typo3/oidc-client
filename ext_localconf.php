<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

defined('TYPO3') or die();

use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

(static function() {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $settings = \IMATHUZH\OidcClient\Service\Configuration::getInstance();
    $lll = "LLL:EXT:$extkey/Resources/Private/Language/locallang.xlf";

    // TypoScript configuration is automatically loaded in Typo3 v12 and higher
    $versionInfo = GeneralUtility::makeInstance(Typo3Version::class);
    if ($versionInfo->getMajorVersion() < 12) {
        ExtensionManagementUtility::addPageTSConfig(
            '@import "EXT:' . $extkey . '/Configuration/page.tsconfig"'
        );
    }

    // Require 3rd-party libraries, in case TYPO3 does not run in composer mode
    $libraries = ExtensionManagementUtility::extPath($extkey) . 'Libraries';
    foreach ([
        'league-oauth2-client.phar' => \League\OAuth2\Client\Provider\AbstractProvider::class,
        'firebase-php-jwt.phar' => \Firebase\JWT\JWT::class
    ] as $file => $class) {
        if (!class_exists($class)) {
            @include "phar://$libraries/$file/vendor/autoload.php";
        }
    }

    // Frontend account plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        $extkey,
        \IMATHUZH\OidcClient\Utility\Constants::PNAME_FEACCOUNT,
        [\IMATHUZH\OidcClient\Controller\FrontendAccountController::class => 'overview,update,link,unlink'],
        [\IMATHUZH\OidcClient\Controller\FrontendAccountController::class => 'overview,update,link,unlink'],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );

    // Backend login provider
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['backend']['loginProviders'][1706879181] = [
        'provider' => \IMATHUZH\OidcClient\Backend\OidcLoginProvider::class,
        'sorting' => 50,
        'iconIdentifier' => \IMATHUZH\OidcClient\Utility\Constants::ICON_PROVIDER,
        'label' => "$lll:belogin.label"
    ];

    // Register classes processing table data for backend displays
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals']
        [\IMATHUZH\OidcClient\Backend\ClaimMappingEval::class] = '';

    // Service configuration
    $subTypes = [];
    if ($settings->frontend()->enabled()) {
        $subTypes[] = 'getUserFE,authUserFE,getGroupsFE';
    }
    if ($settings->backend()->enabled()) {
        $subTypes[] = 'getUserBE,authUserBE,getGroupsBE';
    }
    // Register the service only when at least one type of the authentication is enabled
    if ($subTypes) {
        $authenticationClassName = \IMATHUZH\OidcClient\Authentication\AuthenticationService::class;
        ExtensionManagementUtility::addService(
            $extkey,
            'auth' /* sv type */,
            $authenticationClassName /* sv key */,
            [
                'title' => 'Authentication service',
                'description' => 'Authentication with OpenID Connect providers',
                'subtype' => implode(',', $subTypes),
                'available' => true,
                'priority' => $settings->authServicePriority(),
                'quality' => $settings->authServiceQuality(),
                'os' => '',
                'exec' => '',
                'className' => $authenticationClassName,
            ]
        );
    }

})();
