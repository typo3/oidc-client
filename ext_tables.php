<?php
declare(strict_types=1);

/**
 * Multi OpenID Connect client for Typo3
 * Krzysztof K. Putyra
 * support@math.uzh.ch
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

defined('TYPO3') or die();

(static function() {

    $extkey = \IMATHUZH\OidcClient\Utility\Constants::EXT_KEY;
    $extname = \IMATHUZH\OidcClient\Utility\Constants::EXT_NAME;
    $modname = \IMATHUZH\OidcClient\Utility\Constants::MOD_NAME;

    // Add the module to the bottom of the web main module
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        $extname,
        'web',
        $modname,
        'bottom',
        [
            \IMATHUZH\OidcClient\Controller\BackendController::class => 'overview,update'
        ], [
            'access' => 'admin',
            'name' => "web_$modname",
            'iconIdentifier' =>  \IMATHUZH\OidcClient\Utility\Constants::ICON_MODULE,
            'labels' => "LLL:EXT:$extkey/Resources/Private/Language/locallang.xlf"
        ]
    );

})();
