CREATE TABLE tx_oidcclient_config (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    disable tinyint(4) DEFAULT '0' NOT NULL,
    sorting tinyint(4) DEFAULT '0' NOT NULL,

    -- OIDC endpoints
    auth_endpoint  varchar(255) DEFAULT '' NOT NULL,
    token_endpoint varchar(255) DEFAULT '' NOT NULL,
    userinfo_endpoint varchar(255) DEFAULT '' NOT NULL,
    logout_endpoint varchar(255) DEFAULT '' NOT NULL,
    revoke_endpoint varchar(255) DEFAULT '' NOT NULL,

    -- Request options
    callback_forwarding tinyint(4) DEFAULT '0' NOT NULL,
    lang_parameter varchar(63) DEFAULT 'language' NOT NULL,

    -- OIDC client configuration
    client_id varchar(127) DEFAULT '' NOT NULL,
    client_secret varchar(127) DEFAULT '' NOT NULL,
    client_scopes varchar(127) DEFAULT '' NOT NULL,
    mapping text DEFAULT NULL,
    id_claim varchar(31) DEFAULT 'sub' NOT NULL,
    processors text DEFAULT NULL,

    -- Authentication features
    backend tinyint(3) unsigned DEFAULT 3 NOT NULL,
    frontend tinyint(3) unsigned DEFAULT 3 NOT NULL,
    mfa tinyint(3) unsigned DEFAULT 1 NOT NULL,
    be_groups varchar(127) DEFAULT '' NOT NULL,
    fe_groups varchar(127) DEFAULT '' NOT NULL,

    -- Visual data
    logo      int(11) unsigned DEFAULT '0' NOT NULL,
    name      varchar(31) NOT NULL UNIQUE,
    title     varchar(63) NOT NULL,
    description text DEFAULT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid)
);

CREATE TABLE tx_oidcclient_session (
    sesid char(8) NOT NULL,
    state char(32) NOT NULL,
    token char(32) NOT NULL,
    tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    data blob NOT NULL,

    PRIMARY KEY (sesid),
    UNIQUE state (state),
    KEY tstamp (tstamp)
);

CREATE TABLE tx_oidcclient_feuser (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    disable tinyint(4) DEFAULT '0' NOT NULL,

    provider int(11) UNSIGNED NOT NULL,
    sub varchar(127) NOT NULL,
    user int(11) UNSIGNED NOT NULL,
    email varchar(127) DEFAULT NULL,
    last_auth int(11) NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    UNIQUE oidc (provider,sub),
    KEY fk_provider (provider),
    KEY fk_user (user)
);

CREATE TABLE tx_oidcclient_fegroup (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    disable tinyint(4) DEFAULT '0' NOT NULL,

    provider int(11) UNSIGNED NOT NULL,
    claim varchar(40) NOT NULL,
    pattern varchar(255) NOT NULL,
    group int(11) UNSIGNED NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY fk_provider (provider),
    KEY fk_group (group)
);

CREATE TABLE tx_oidcclient_beuser (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    disable tinyint(4) DEFAULT '0' NOT NULL,

    provider int(11) UNSIGNED NOT NULL,
    sub varchar(127) NOT NULL,
    user int(11) UNSIGNED NOT NULL,
    email varchar(127) DEFAULT NULL,
    last_auth int(11) NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    UNIQUE oidc (provider,sub),
    KEY fk_provider (provider),
    KEY fk_user (user)
);

CREATE TABLE tx_oidcclient_begroup (
    uid int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT '0' NOT NULL,

    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    disable tinyint(4) DEFAULT '0' NOT NULL,

    provider int(11) UNSIGNED NOT NULL,
    claim varchar(40) NOT NULL,
    pattern varchar(255) NOT NULL,
    group int(11) UNSIGNED NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY fk_provider (provider),
    KEY fk_group (group)
);

-- Extension required by DBAL for referring child records
CREATE TABLE fe_users (
    tx_oidcclient int(11) DEFAULT '0' NOT NULL
);

CREATE TABLE be_users (
    tx_oidcclient int(11) DEFAULT '0' NOT NULL
);

CREATE TABLE fe_groups (
    tx_oidcclient_pattern int(11) DEFAULT '0' NOT NULL
);

CREATE TABLE be_groups (
    tx_oidcclient_pattern int(11) DEFAULT '0' NOT NULL
);